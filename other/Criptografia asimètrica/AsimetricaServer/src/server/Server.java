/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

/**
 *
 *
 */
public class Server {

    /**
     * @param args the command line arguments
     * @throws java.io.IOException
     */
    public static void main(String[] args) throws IOException {
        System.out.println("Bienvenido al servidor. Este programa envía claves publicas y espera recibir mensajes de los clientes. ");
        System.out.println("Éstos mensajes serán seguros y sólo el server podrá descifrarlos.");
        System.out.println("");
        ServerSocket ss = new ServerSocket(12345);
        Socket s;
        while (true) {
            s = ss.accept();
            (new Connexio(s)).start();
        }

    }

    private static class Connexio extends Thread {

        private Socket s;
        private ObjectInputStream ois;
        private ObjectOutputStream oos;

        public Connexio(Socket s) {
            this.s = s;
        }

        @Override
        public void run() {
            try {
                // Declaraciones de conexión.
                System.out.println("Nueva conexión en curso. Cliente: " + s.getInetAddress());
                ois = new ObjectInputStream(s.getInputStream());
                oos = new ObjectOutputStream(s.getOutputStream());

                // Se crean las claves: publica y privada
                KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA");
                keyGen.initialize(1024);
                KeyPair keypair = keyGen.generateKeyPair();
                PrivateKey claveprivada = keypair.getPrivate();
                PublicKey clavepublica = keypair.getPublic();

                // Envia la clave pulica al cliente
                oos.writeObject((PublicKey) clavepublica);

                //Recibimos claveSecretaEncriptada y mensajeCifrado
                byte[] claveSecretaEncriptada = (byte[]) ois.readObject();
                byte[] mensajeCifrado = (byte[]) ois.readObject();
                System.out.println("Clave secreta recibida.");
                System.out.println("Mensaje cifrado recibido: " + new String(mensajeCifrado));
                System.out.println("Procedemos a la desencriptación del mensaje...");
                System.out.println("");

                // Se desencripta la clave secreta con la clave privada.
                Cipher encriptador = Cipher.getInstance("RSA/ECB/PKCS1Padding");
                encriptador.init(Cipher.UNWRAP_MODE, claveprivada);
                Key claveSecretaDesencriptada = encriptador.unwrap(claveSecretaEncriptada, "AES", Cipher.SECRET_KEY);

                // Ahora que tenemos la clave secreta, desencriptamos el mensaje
                encriptador = Cipher.getInstance("AES/ECB/PKCS5Padding");
                encriptador.init(Cipher.DECRYPT_MODE, claveSecretaDesencriptada);
                byte[] desencriptado = encriptador.doFinal(mensajeCifrado);

                //Salida de solución
                System.out.println("Mensaje descifrado con éxito: " + new String(desencriptado));
                System.out.println("Cerrando conexión con el cliente " + s.getInetAddress());
            } catch (IOException ex) {
            } catch (NoSuchAlgorithmException | ClassNotFoundException | NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException | BadPaddingException ex) {
                Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                try {
                    if (oos != null) {
                        oos.close();
                    }
                    if (ois != null) {
                        oos.close();
                    }
                    if (s != null) {
                        oos.close();
                    }
                } catch (IOException e) {
                }
            }
        }
    }
}
