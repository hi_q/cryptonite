/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cliente;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.util.Scanner;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;

/**
 *
 *
 */
public class Cliente {

    /**
     * @param args the command line arguments
     * @throws java.io.IOException
     * @throws java.security.NoSuchAlgorithmException
     * @throws javax.crypto.NoSuchPaddingException
     * @throws java.security.InvalidKeyException
     * @throws javax.crypto.IllegalBlockSizeException
     */
    public static void main(String[] args) throws IOException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
        //declaraciones iniciales
        Scanner entrada = new Scanner(System.in);
        ObjectInputStream ois = null;
        ObjectOutputStream oos = null;
        Socket s = null;
        System.out.println("Bienvenido. Este servicio te permite enviar un mensaje SEGURO al server.");
        System.out.print("Escribe tu mensaje = ");
        String texto = entrada.nextLine();
        System.out.println("Mensaje recibido. Esperando servidor para conectar...");
        System.out.println("");
        try {
            //String address = "192.168.1.43"; //o  IP localhost 127.0.0.1
            String address = "127.0.0.1";
            s = new Socket(address, 12345);
            oos = new ObjectOutputStream(s.getOutputStream());
            ois = new ObjectInputStream(s.getInputStream());

            // recibimos la clave publica del server
            PublicKey clavepublica = (PublicKey) ois.readObject();

            // Clave secreta AES
            KeyGenerator kg = KeyGenerator.getInstance("AES");
            kg.init(128);
            SecretKey claveSecreta = kg.generateKey();

            // Se encripta la clave secreta con la clave publica del server.
            Cipher encriptador = Cipher.getInstance("RSA/ECB/PKCS1Padding");
            encriptador.init(Cipher.WRAP_MODE, clavepublica);
            byte[] claveSecretaEncriptada = encriptador.wrap(claveSecreta);

            //Ciframos el texto con la clave secreta
            encriptador = Cipher.getInstance("AES/ECB/PKCS5Padding");
            encriptador.init(Cipher.ENCRYPT_MODE, claveSecreta);
            byte[] mensaje = texto.getBytes();
            byte[] mensajeCifrado = encriptador.doFinal(mensaje);

            // Se envía el claveSecretaEncriptada
            oos.writeObject(claveSecretaEncriptada);
            // Se envía el mensajeCifrado
            oos.writeObject(mensajeCifrado);
            
            System.out.println("");
            System.out.println("Mensaje enviado, hasta la próxima.");
        } catch (IOException | ClassNotFoundException e) {
            System.err.println(e);
        } finally {
            if (oos != null) {
                oos.close();
            }
            if (ois != null) {
                ois.close();
            }
            if (s != null) {
                s.close();
            }
        }
    }
}
