package ClienteSSLKripto;

import ClienteSSLKripto.hashutils.HashAlgorithm;
import ClienteSSLKripto.hashutils.HashProcess;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import java.util.Scanner;

public class Cliente {

    private static final String[] ARRAYALGORITMOS = {"MD5", "SHA-1", "SHA-256", "SHA-512"};

    public static void main(String[] args) throws IOException {
        Scanner entrada = new Scanner(System.in);
        try {
            String Host = "127.0.0.1";
            int port = 6000;
            System.setProperty("javax.net.ssl.trustStore", "cert/ClientSSLKeyStore");
            System.setProperty("javax.net.ssl.trustStorePassword", "123456");
            // Obtener el SocketSSL utilitzant el patron factory y establecer el flujo de salida al servidor
            SSLSocketFactory sfact = (SSLSocketFactory) SSLSocketFactory.getDefault();
            SSLSocket cliente = (SSLSocket) sfact.createSocket(Host, port);
            ObjectOutputStream fluxSortida = new ObjectOutputStream(cliente.getOutputStream());
            ObjectInputStream fluxEntrada = new ObjectInputStream(cliente.getInputStream());
            //Inicio del poceso
            System.out.println("Servicio inicializado.");
            System.out.print("¿Cual es tu nombre?: ");
            String nombre = entrada.nextLine();
            System.out.println("Entendido, " + nombre + ". Esperando mensaje del servidor...");
            //Hacemos saludo
            fluxSortida.writeObject(nombre); // WRITE 1
            //recibimos respuesta
            String algoritmo = (String) fluxEntrada.readObject();
            HashAlgorithm algoritmoElegido = HashAlgorithm.findByName(algoritmo);
            String mensaje = (String) fluxEntrada.readObject();
            System.out.println("Mensaje recibido: " + mensaje + ". Algoritmo: " + algoritmoElegido.getLongName() + ".");
            System.out.println("");
            System.out.print("¿Alterar mensaje? (0 no), (1 sí): ");
            int opcion = numeroErrores(0, 1);
            if (opcion == 1) {
                System.out.print("Escribe mensaje nuevo: ");
                mensaje = entrada.nextLine();
            }
            //Aplicando hash
            HashProcess calculate = new HashProcess(mensaje, algoritmoElegido);
            calculate.process();
            String resultado = calculate.getMessageEncodedAsString(); // Tanquem recursos
            //enviando hash
            fluxSortida.writeObject(resultado); // WRITE 2
            System.out.println("Hash obtenido del mensaje: " + resultado + ".");
            System.out.println("Enviando hash al servidor. Programa finalizado.");
            //cerramos todo
            fluxEntrada.close();
            fluxSortida.close();
            cliente.close();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Cliente.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static int numeroErrores(int origin, int bound) {
        Scanner k = new Scanner(System.in);
        Integer var;
        while (true) {
            if (k.hasNextInt()) {
                var = k.nextInt();
                if (var >= origin && var <= bound) {
                    return var;
                } else {
                    System.err.println("No esta en la lista de opciones.");
                }
            } else {
                System.err.println("No es un numero.");
            }
            k.nextLine();
        }
    }
}
