/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ServidorSSLKripto.hashutils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Luciano
 */
public class HashProcess extends Process<HashAlgorithm> implements Generation{

    public HashProcess(String message, HashAlgorithm algorithm) {
        super(message, algorithm);
    }

    public HashProcess(byte[] message, HashAlgorithm algorithm) {
        super(message, algorithm);
    }

    
    
    @Override
    public void process(){
        if (message != null && algorithm != null) {
            MessageDigest messageDigest = null;
            try{
                messageDigest = MessageDigest.getInstance(algorithm.getName());
                messageDigest.update(message);
                messageEncoded = messageDigest.digest();
            } catch (NoSuchAlgorithmException ex) {
                Logger.getLogger(HashProcess.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        else{
            System.err.println("WARNING. Could'nt digest. Hash processor has null values.");
        }
    }
    
    public static boolean isValid(String message, String hash, HashAlgorithm against){
        HashProcess process = new HashProcess(message,against);
        process.process();
        String check = process.getMessageEncodedAsString();
        return check.equals(hash);
    }
}
