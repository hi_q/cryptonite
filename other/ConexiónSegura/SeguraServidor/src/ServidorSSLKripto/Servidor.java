/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ServidorSSLKripto;

import ServidorSSLKripto.hashutils.HashAlgorithm;
import ServidorSSLKripto.hashutils.HashProcess;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.net.ServerSocketFactory;
import javax.net.ssl.SSLServerSocketFactory;
import javax.net.ssl.SSLSocket;
import java.util.Scanner;

public class Servidor extends Thread {

    private final SSLSocket socket;
    private final String[] ARRAYALGORITMOS = {"MD5", "SHA-1", "SHA-256", "SHA-512"};

    public static void main(String[] args) throws Exception {
        System.setProperty("javax.net.ssl.keyStore", "cert/ServerSSLKeyStore");//Almacén de certificado en la raiz.
        System.setProperty("javax.net.ssl.keyStorePassword", "123456");

        ServerSocketFactory ssfactory = SSLServerSocketFactory.getDefault();
        try (ServerSocket serversocket = ssfactory.createServerSocket(6000)) {
            int con = 0;
            while (con < 2) {
                System.out.println("Esperando conexión nº: " + con);
                new Servidor((SSLSocket) serversocket.accept()).start();
                con++;
            }
        }
    }

    public Servidor(SSLSocket s) {
        socket = s;
    }

    @Override
    public void run() {

        Scanner entrada = new Scanner(System.in);
        try {
            try (ObjectInputStream fentrada = new ObjectInputStream(socket.getInputStream())) {
                ObjectOutputStream fsalida = new ObjectOutputStream(socket.getOutputStream());
                //mensaje del cliente inicial (nombre)
                String msg = (String) fentrada.readObject(); // READ 1
                System.out.println("El usuario " + msg + " está conectado.");
                System.out.println("______________________________________________");
                HashAlgorithm[] values = HashAlgorithm.values();
                for (int i = 0; i < values.length; i++) {
                    String hash = values[i].getLongName();
                    System.out.println(i + ": " + hash);
                }
                System.out.println("______________________________________________");

                //suceso de repetición
                System.out.print("Qué algoritmo se usará para la conversación?: ");
                int algoritmo = numeroErrores(0, 3);
                System.out.println("");
                System.out.print("Escribir el mensaje protegido: ");
                String mensaje = entrada.nextLine();
                System.out.println("");
                HashAlgorithm algoritmoElegido = values[algoritmo];
                System.out.println("Mensaje a enviar: " + mensaje + ". Algoritmo utilizado: " + algoritmoElegido.getLongName() + ".");
                String hash = obtenerHash(mensaje, values[algoritmo]);
                //enviamos mensaje al cliente
                fsalida.writeObject(algoritmoElegido.getName()); // WRITE 1 
                fsalida.writeObject(mensaje); // WRITE 2 
                System.out.println("Enviado, esperando respuesta del cliente para confirmar su hash...");
                System.out.println("");
                String hash2 = (String) fentrada.readObject(); // READ 2
                System.out.println("Hash recibido:" + hash2);
                System.out.println("Hash original: " + hash);
                if (hash.equalsIgnoreCase(hash2)) {
                    System.out.println("Los Hash son iguales, el mensaje no ha sido alterado.");
                } else {
                    System.out.println("El mensaje ha sido alterado, su contenido no es fiable.");
                }
                //Cerrar
                fsalida.close();
                socket.close();
            }
        } catch (IOException ex) {
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Servidor.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private static String obtenerHash(String mensaje, HashAlgorithm algoritmo) {
        HashProcess calculate = new HashProcess(mensaje, algoritmo);
        calculate.process();
        String resultado = calculate.getMessageEncodedAsString();
        return resultado;
    }

    public static int numeroErrores(int origin, int bound) {
        Scanner k = new Scanner(System.in);
        Integer var;
        while (true) {
            if (k.hasNextInt()) {
                var = k.nextInt();
                if (var >= origin && var <= bound) {
                    return var;
                } else {
                    System.err.println("No esta en la lista de opciones.");
                }
            } else {
                System.err.println("No es un numero.");
            }
            k.nextLine();
        }
    }

}
