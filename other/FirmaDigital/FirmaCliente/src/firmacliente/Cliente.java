/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package firmacliente;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.Signature;
import java.security.SignatureException;

/**
 *
 *
 */
public class Cliente {

    /**
     * @param args the command line arguments
     * @throws java.io.IOException
     * @throws java.security.InvalidKeyException
     * @throws java.security.NoSuchAlgorithmException
     * @throws java.security.SignatureException
     */
    public static void main(String[] args) throws IOException, InvalidKeyException, NoSuchAlgorithmException, SignatureException {
        //Declaraciones
        ObjectInputStream ois = null;
        ObjectOutputStream oos = null;
        Socket socket = null;
        try {
            //Se realiza la conexión con el servidor
            String address = "127.0.0.1";
            socket = new Socket(address, 12345);
            oos = new ObjectOutputStream(socket.getOutputStream()); 
            ois = new ObjectInputStream(socket.getInputStream()); 
            // Esperamos a recibir todos los datos necesarios del server
            System.out.println("Esperando recibir datos...");
            byte[] firma = (byte[]) ois.readObject(); //el "objeto" firmado (con la clave privada que NO tenemos)
            PublicKey ClavPublica = (PublicKey) ois.readObject(); // la llave pública
            String missatge = (String) ois.readObject(); //"objeto" básico
            //Se crea un objeto firma y se le asocia una clave publica
            Signature firmaClient = Signature.getInstance("SHA1withDSA");
            firmaClient.initVerify(ClavPublica);
            // A la firma se le proporcionan los datos a comprobar
            firmaClient.update(missatge.getBytes());
            boolean comprobador = firmaClient.verify(firma);
            //Realiza la comprobación con la clave publica de si el mensaje firmado es genuino del server (privada).
            if (comprobador == true) {
                System.out.println("Firma verificada satisfactoriamente.");
            } else {
                System.out.println("Firma no verificada...");
            }
            System.out.println("");

        } catch (IOException | ClassNotFoundException e) {
            System.err.println(e);
        } finally {
            if (oos != null) {
                oos.close();
            }
            if (ois != null) {
                ois.close();
            }
            if (socket != null) {
                socket.close();
            }
        }
    }
}
