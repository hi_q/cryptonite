/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package firmaserver;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Signature;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 *
 */
public class Server {

    /**
     * @throws java.io.IOException
     */
    public static PrivateKey ClavPrivada;
    public static PublicKey ClavPublica;

    public static void main(String[] args) throws IOException {
        ServerSocket ss = new ServerSocket(12345);
        Socket soket;
        while (true) {
            soket = ss.accept();
            (new Conexion(soket)).start();
        }
    }

    private static class Conexion extends Thread {

        private final Socket soket;
        private ObjectInputStream ois;
        private ObjectOutputStream oos;

        public Conexion(Socket soket) {
            this.soket = soket;
        }

        @Override
        public void run() {
            try {
                // Muestra IP del cliente y enmascaramos la entrada y salida de bytes.
                System.out.println("Conexión entrante: " + soket.getInetAddress());
                ois = new ObjectInputStream(soket.getInputStream());
                oos = new ObjectOutputStream(soket.getOutputStream());
                // Declaramos las claves, y miramos si ya existen o no: Si existen, las cargamos, si no, se crean
                obtenerClaves();
                // Se crea la firma y se le asocia la clave privada.
                Signature firmaServer = Signature.getInstance("SHA1withDSA");
                firmaServer.initSign(ClavPrivada);
                // A la firma se le proporciona los datos que debe firmar y firmamos el mensaje.
                String missatge = "Mensaje secreto de Michael: Merezco un estupendo 10.";
                firmaServer.update(missatge.getBytes());
                byte[] firma = firmaServer.sign();
                // Enviamos al cliente todo lo que necesita (NO la clave privada)
                oos.writeObject(firma); //mensaje firmado
                oos.writeObject(ClavPublica); //clave publica
                oos.writeObject(missatge); //mensaje original
                System.out.println("Enviando información necesaria al cliente. Cerrada conexión con el mismo.");
            } catch (IOException ex) {
            } catch (NoSuchAlgorithmException | SignatureException | InvalidKeyException ex) {
                Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                try {
                    if (oos != null) {
                        oos.close();
                    }
                    if (ois != null) {
                        oos.close();
                    }
                    if (soket != null) {
                        oos.close();
                    }
                } catch (IOException e) {
                }
            }
        }

        public void obtenerClaves() {
            try {
                File Clave = new File("Clave.privada");
                if (Clave.exists()) {
                    // Instància de Keyfactory, para convertir claves de formato criptográfico
                    KeyFactory keyDSA = KeyFactory.getInstance("DSA");
                    // Lectura del archivo de la clave PRIVADA en bytes
                    FileInputStream lecturaPrivada = new FileInputStream("Clave.privada");
                    byte[] bufferPriv = new byte[lecturaPrivada.available()];
                    lecturaPrivada.read(bufferPriv);
                    lecturaPrivada.close();
                    // Recupera la clave privada (PKCS8)
                    PKCS8EncodedKeySpec clavePrivadaSpec = new PKCS8EncodedKeySpec(bufferPriv);
                    ClavPrivada = keyDSA.generatePrivate(clavePrivadaSpec);
                    // Lectura del archivo de la clave PUBLICA en bytes.
                    FileInputStream lecturaPublica = new FileInputStream("Clave.publica"); // Lectura fitxer
                    byte[] bufferPub = new byte[lecturaPublica.available()];
                    lecturaPublica.read(bufferPub);
                    lecturaPublica.close();
                    // Recupera la clave publica (X.509)
                    X509EncodedKeySpec clavePublicaSpec = new X509EncodedKeySpec(bufferPub);
                    ClavPublica = keyDSA.generatePublic(clavePublicaSpec);
                    System.out.println("Claves recuperadas con éxito.");
                    System.out.println("");
                } else {
                    //obtenemos el generador de claves.
                    KeyPairGenerator keyGen = KeyPairGenerator.getInstance("DSA");
                    //Iniciamos el generador de claves.
                    SecureRandom numero = SecureRandom.getInstance("SHA1PRNG");
                    keyGen.initialize(1024, numero);
                    // Se generan la clave privada y publica (relación directa).
                    KeyPair par = keyGen.generateKeyPair();
                    ClavPrivada = par.getPrivate();
                    ClavPublica = par.getPublic();
                    //Guardamos las claves en la raiz, para posteriores usos.
                    //privada
                    PKCS8EncodedKeySpec pk8Spec = new PKCS8EncodedKeySpec(ClavPrivada.getEncoded());
                    //pasamos la clave a binario
                    FileOutputStream exportarPrivada = new FileOutputStream("Clave.privada");
                    exportarPrivada.write(pk8Spec.getEncoded());
                    exportarPrivada.close();
                    //publica
                    X509EncodedKeySpec pkX509 = new X509EncodedKeySpec(ClavPublica.getEncoded());
                    //Escriure a binari la clau pública
                    FileOutputStream exportarPublica = new FileOutputStream("Clave.publica");
                    exportarPublica.write(pkX509.getEncoded());
                    exportarPublica.close();
                    System.out.println("Creadas claves con éxito.");
                    System.out.println("");
                }
            } catch (IOException ex) {
            } catch (NoSuchAlgorithmException | InvalidKeySpecException ex) {
                Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
            } 
        }
    }
}
