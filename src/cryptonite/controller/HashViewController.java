/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cryptonite.controller;

import cryptonite.Cryptonite;
import cryptonite.controller.utils.CryptoniteFileExtension;
import cryptonite.controller.utils.CryptoniteSimpleFileChooser;
import cryptonite.controller.utils.SimpleDialog;
import cryptonite.utils.hashutils.HashAlgorithm;
import cryptonite.res.R;
import cryptonite.utils.hashutils.HashProcess;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.RandomAccessFile;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.ScaleTransition;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.RadioButton;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.paint.Color;
import javafx.util.Duration;

/**
 *
 * @author Luciano
 */
public class HashViewController implements Initializable{

    private @FXML AnchorPane mainPanel;
    private @FXML SplitPane mainSplitPanel;
    
    private @FXML AnchorPane bannerPanel;
    
    private @FXML ImageView backImage;
    private @FXML ImageView exitImage;
    
    // SECCION ALGORITMOS HASH
    private @FXML FlowPane algorithmFlowPanel;
    private @FXML Label algorithmLabel;
    private ToggleGroup algorithmButtons;
    private HashAlgorithm currentAlgorithm;
    
    
    // SECCION GENERAR
    private @FXML AnchorPane generatePanel;
    private @FXML Label generateLabel;
    private @FXML Button generateButton;
    private @FXML Button generateCleanButton;
    private ToggleGroup originRadioButtons;
    private File currentGenerateOriginFile;
    private File currentGenerateTargetFile;
    
        // GENERAR - IZQUIERDA
        private @FXML RadioButton fileOriginRButton;
        private @FXML RadioButton textOriginRButton;
        private @FXML TextField fileOriginTextField;
        private @FXML TextArea textOriginTextArea;
        private @FXML Button chooseFileOriginButton;
    
        // GENERAR - DERECHA
        private @FXML CheckBox fileTargetCheckBox;
        private @FXML CheckBox textTargetCheckBox;
        private @FXML TextField fileTargetTextField;
        private @FXML TextArea textTargetTextArea;
        private @FXML Button chooseFileTargetButton;
        
    // SECCION VALIDAR
    private @FXML AnchorPane validatePanel;
    private @FXML Button validateButton;
    private @FXML Button validateCleanButton;
    private @FXML Label validateLabel;
    private ToggleGroup validateOriginRadioButtons;
    private ToggleGroup validateTargetRadioButtons;
    private File currentValidateOriginFile;
    private File currentValidateTargetFile;
    private @FXML Label validateInfoLabel;
    
        // VALIDAR - IZQUIERDA
        private @FXML RadioButton validateFileOriginRButton;
        private @FXML RadioButton validateTextOriginRButton;
        private @FXML TextArea validateTextOriginTextArea;
        private @FXML TextField validateFileOriginTextField;
        private @FXML Button validateChooseFileOriginButton;
        
        // VALIDAR - DERECHA
        private @FXML TextField validateFileTargetTextField;
        private @FXML TextArea validateTextTargetTextArea;
        private @FXML Button validateChooseFileTargetButton;
        private @FXML RadioButton validateFileTargetRButton;
        private @FXML RadioButton validateTextTargetRButton;
    
    
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        initBanner();
        initDisableDivider();
        initBackImage();
        initExitImage();
        disableSections();
        initAlgorithmSection();
        initGenerateSection();
        initValidateSection();
        stretchOpenTransition();
    }
    
    // FASES DE LA INICIALIZACION
    public void initDisableDivider() {
        mainSplitPanel.lookupAll(".split-pane-divider").stream()
            .forEach(div ->  div.setMouseTransparent(true) );
    }
    private double xOffsetTemporal;
    private double yOffsetTemporal;
    public void initBanner(){ // banner se mueve
        R.getImage("banner").setAsBackgroundCover(bannerPanel);
        bannerPanel.getStyleClass().add("posDown");
        
        // METODOS ARRASTRAR VENTANA
        bannerPanel.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                xOffsetTemporal = Cryptonite.getStage().getX() - event.getScreenX();
                yOffsetTemporal = Cryptonite.getStage().getY() - event.getScreenY();
            }
        });
        bannerPanel.setOnMouseDragged(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                Cryptonite.getStage().setX(event.getScreenX() + xOffsetTemporal);
                Cryptonite.getStage().setY(event.getScreenY() + yOffsetTemporal);
            }
        });
    }
    public void initBackImage(){
        R.getImage("back").setAsImage(backImage);
        backImage.setOnMouseClicked(this::onBackClick);
    }
    public void initExitImage(){
        R.getImage("cross").setAsImage(exitImage);
        exitImage.setOnMouseClicked(this::onExitClick);
    }
    public void initAlgorithmSection(){
        initAlgorithmLabel();
        initAlgorithmPanel();
    } /*se invoca initSection para invocar a sus subyacentes resultando en menos lineas dentro del initialize*/
        public void initAlgorithmLabel(){
            algorithmLabel.setText(R.getString("use_algorithm"));
        }
        public void initAlgorithmPanel(){
            ObservableList<Node> childs = algorithmFlowPanel.getChildren();
            algorithmButtons = new ToggleGroup();
            ObservableList<Toggle> radioGroup = algorithmButtons.getToggles();
            for (HashAlgorithm algorithm : HashAlgorithm.values()) {
                RadioButton button = new RadioButton();
                button.setId(algorithm.getName());
                button.setOnAction(this::onAlgorithmSelect);
                button.setText(algorithm.getName());
                childs.add(button);
                radioGroup.add(button);
            }
        }
    public void initGenerateSection(){
        initGenerateLabel();
        initGenerateOriginTextContent();
        initGenerateRadioButtons();
        initGenerateCheckBoxes();
        initGenerateButton();
        initGenerateChooseButton();
        initGenerateCleanButton();
        initFirstOriginSelection();
    }
        public void initGenerateLabel(){
            generateLabel.setText(R.getString("generate_hash_title"));
        }
        public void initGenerateOriginTextContent(){
            fileTargetCheckBox.setText(R.getString("file"));
            textTargetCheckBox.setText(R.getString("text"));
            fileOriginRButton.setText(R.getString("file"));
            textOriginRButton.setText(R.getString("text"));
        }
        public void initGenerateRadioButtons(){
            originRadioButtons = new ToggleGroup();
            ObservableList<Toggle> originRadioButtonsContent = originRadioButtons.getToggles();
            originRadioButtonsContent.add(fileOriginRButton);
            originRadioButtonsContent.add(textOriginRButton);
            
            fileOriginRButton.setOnAction(this::onFileOriginRadioButtonClick);
            textOriginRButton.setOnAction(this::onTextOriginRadioButtonClick);
        }
        public void initGenerateCheckBoxes(){
            fileTargetCheckBox.setOnAction(this::onFileTargetCheckBoxClick);
            textTargetCheckBox.setOnAction(this::onTextTargetCheckBoxClick);
            disableTargetFile();
            disableTargetText();
        }
        public void initGenerateButton(){
            R.getImage("hash_logo").setAsGraphic(generateButton);
            generateButton.setText(R.getString("generate_hash"));
            generateButton.setOnAction(this::onGenerateButtonClick);
        }
        public void initGenerateChooseButton() {
            chooseFileOriginButton.setOnAction(this::onChooseFileOriginClick);
            chooseFileTargetButton.setOnAction(this::onChooseFileTargetClick);
            R.getImage("find").setAsBackgroundContained(chooseFileTargetButton);
            R.getImage("find").setAsBackgroundContained(chooseFileOriginButton);
        }
        public void initGenerateCleanButton() {
            generateCleanButton.setText(R.getString("clean"));
            generateCleanButton.setOnAction(this::onGenerateCleanButtonClick);
        }
        public void initFirstOriginSelection(){
            fileOriginRButton.setSelected(true);
            onFileOriginRadioButtonClick(null);
        }
    public void initValidateSection(){
        initValidateLabel();
        initValidateButton();
        initValidateCleanButton();
        initValidateChooseButtons();
        initValidateOriginTextContent();
        initValidateOriginRadioButtons();
        initValidateTargetTextContent();
        initValidateTargetRadioButtons();
        initFirstValidateOriginSelection();
    }
        public void initValidateLabel(){
            validateLabel.setText(R.getString("validate_hash_title"));
        }
        public void initValidateButton(){
            R.getImage("valid_logo").setAsGraphic(validateButton);
            validateButton.setText(R.getString("validate_hash"));
            validateButton.setOnAction(this::onValidateButtonClick);
        }
        public void initValidateCleanButton() {
            validateCleanButton.setText(R.getString("clean"));
            validateCleanButton.setOnAction(this::onValidateCleanButtonClick);
            validateInfoLabel.setVisible(false);
        }
        public void initValidateChooseButtons() {
            validateChooseFileOriginButton.setOnAction(this::onValidateChooseFileOriginClick);
            validateChooseFileTargetButton.setOnAction(this::onValidateChooseFileTargetClick);
            R.getImage("find").setAsBackgroundContained(validateChooseFileOriginButton);
            R.getImage("find").setAsBackgroundContained(validateChooseFileTargetButton);
        }
        public void initValidateOriginTextContent(){
            validateFileOriginRButton.setText(R.getString("file"));
            validateTextOriginRButton.setText(R.getString("text"));
        }
        public void initValidateOriginRadioButtons(){
            validateOriginRadioButtons = new ToggleGroup();
            ObservableList<Toggle> originRadioButtonsContent = validateOriginRadioButtons.getToggles();
            originRadioButtonsContent.add(validateFileOriginRButton);
            originRadioButtonsContent.add(validateTextOriginRButton);
            
            validateFileOriginRButton.setOnAction(this::onValidateFileOriginRadioButtonClick);
            validateTextOriginRButton.setOnAction(this::onValidateTextOriginRadioButtonClick);
        }
        public void initValidateTargetTextContent(){
            validateFileTargetRButton.setText(R.getString("file"));
            validateTextTargetRButton.setText(R.getString("text"));
        }
        public void initValidateTargetRadioButtons(){
            validateTargetRadioButtons = new ToggleGroup();
            ObservableList<Toggle> originRadioButtonsContent = validateTargetRadioButtons.getToggles();
            originRadioButtonsContent.add(validateFileTargetRButton);
            originRadioButtonsContent.add(validateTextTargetRButton);
            
            validateFileTargetRButton.setOnAction(this::onValidateFileTargetRadioButtonClick);
            validateTextTargetRButton.setOnAction(this::onValidateTextTargetRadioButtonClick);
        }
        public void initFirstValidateOriginSelection(){
            validateFileOriginRButton.setSelected(true);
            validateFileTargetRButton.setSelected(true);
            validateTextOriginTextArea.setVisible(false);
            validateTextTargetTextArea.setVisible(false);
        }
    
    // ACCIONES SEPARADAS
    public void enableSections(){
        generatePanel.setDisable(false);
        validatePanel.setDisable(false);
    }
    public void disableSections(){
        generatePanel.setDisable(true);
        validatePanel.setDisable(true);
    }
    public void cleanGenerateSection() {
        fileOriginTextField.clear();
        textOriginTextArea.clear();
        fileTargetTextField.clear();
        textTargetTextArea.clear();
        currentGenerateOriginFile = null;
        currentGenerateTargetFile = null;
    }
    public void enableGenerateButton(){
        generateButton.setDisable(false);
    }
    public void disableGenerateButton(){
        generateButton.setDisable(true);
    }
    public void enableTargetFile(){
        fileTargetTextField.setDisable(false);
        chooseFileTargetButton.setDisable(false);
        generateButtonCheckAndEnable();
    }
    public void disableTargetFile(){
        fileTargetTextField.setDisable(true);
        chooseFileTargetButton.setDisable(true);
        generateButtonCheckAndEnable();
    }
    public void enableTargetText(){
        textTargetTextArea.setDisable(false);
        generateButtonCheckAndEnable();
    }
    public void disableTargetText(){
        textTargetTextArea.setDisable(true);
        generateButtonCheckAndEnable();
    }
    public void setNewAlgorithm(String name){ // cambiar de algoritmo
        for (HashAlgorithm value : HashAlgorithm.values()) {
            if (name.equals(value.getName())) {
                cleanAlgorithmicDependentFields();
                currentAlgorithm = value;
                return;
            }
        }
    }
    public void cleanAlgorithmicDependentFields() { // limpieza en caso de cambiar de algoritmo
        // lo que haya que limpiar en caso de clicar el radio button de algoritmo
    }
    public String generate(byte[] in, HashAlgorithm algorithm){
        HashProcess process = new HashProcess(in,algorithm);
        process.process();
        return process.getMessageProcessedAsString();
    }
    
    // COMPROBACIONES
    public void generateButtonCheckAndEnable(){
        if (anyGenerateTargetSelected()) enableGenerateButton();
        else disableGenerateButton();
    }
    public boolean anyGenerateTargetSelected(){
        return (fileTargetCheckBox.isSelected() ||
                textTargetCheckBox.isSelected());
    }
    public boolean anyGenerateFieldFilled(){
        // cualquier campo en generate esta rellenado
        if (fileTargetCheckBox.isSelected()
            && !fileTargetTextField.getText().isEmpty())
            return true;
        if (textTargetCheckBox.isSelected()
            && !textTargetTextArea.getText().isEmpty())
            return true;
        if (fileOriginRButton.isSelected()
            && !fileOriginTextField.getText().isEmpty())
            return true;
        if (textOriginRButton.isSelected()
            && !textOriginTextArea.getText().isEmpty())
            return true;
        return false;
    }
    public boolean anyValidateFieldFilled(){
        // cualquier campo en generate esta rellenado
        if (validateFileTargetRButton.isSelected()
            && !validateFileTargetTextField.getText().isEmpty())
            return true;
        if (validateTextTargetRButton.isSelected()
            && !validateTextTargetTextArea.getText().isEmpty())
            return true;
        if (validateFileOriginRButton.isSelected()
            && !validateFileOriginTextField.getText().isEmpty())
            return true;
        if (validateTextOriginRButton.isSelected()
            && !validateTextOriginTextArea.getText().isEmpty())
            return true;
        return false;
    }
    public boolean readyToGenerate(){
        return (fileOriginRButton.isSelected()
            && !fileOriginTextField.getText().isEmpty())
                ||
                (textOriginRButton.isSelected() &&
                !textTargetTextArea.getText().isEmpty());
    }
    public boolean readyToValidate(){
        boolean origin = false;
        boolean target = false;
        if (validateFileOriginRButton.isSelected()) {
            origin = (currentValidateOriginFile != null);
        }
        if (validateFileTargetRButton.isSelected()) {
            target = (currentValidateTargetFile != null);
        }
        if (validateTextOriginRButton.isSelected()) {
            origin = !validateTextOriginTextArea.getText().isEmpty();
        }
        if (validateTextTargetRButton.isSelected()) {
            target = !validateTextTargetTextArea.getText().isEmpty();
        }
        return origin && target;
    }
    
    // TRANSICIONES ABRIR-CERRAR ESCENARIO
    public void stretchOpenTransition(){
        ScaleTransition transition = new ScaleTransition();
        mainPanel.setScaleX(0);
        transition.setNode(mainPanel);
        transition.setDuration(Duration.seconds(0.1));
        transition.setToX(1);
        transition.play();
    }
    public void stretchCloseTransition(EventHandler<ActionEvent> ev){
        ScaleTransition transition = new ScaleTransition();
        transition.setNode(mainPanel);
        transition.setDuration(Duration.seconds(0.1));
        transition.setToX(0);
        transition.play();
        transition.setOnFinished(ev);
    }
    
    //ACCIONES LISTENER
    public void onAlgorithmSelect(ActionEvent e){
        RadioButton origin = (RadioButton)e.getSource();
        String algorithmName = origin.getId();
        currentAlgorithm = HashAlgorithm.findByName(algorithmName);
        enableSections();
    }
    public void onGenerateCleanButtonClick(ActionEvent e) {
        if (anyGenerateFieldFilled()){
            if (SimpleDialog.doQuestion(R.getString("warning"), R.getString("warning_fields_message"))) {
                fileTargetTextField.clear();
                textTargetTextArea.clear();
                fileOriginTextField.clear();
                textOriginTextArea.clear();
                currentGenerateTargetFile = null;
                currentGenerateOriginFile = null;
            }
        }
    }
    public void onGenerateButtonClick(ActionEvent e) {
        String text = null;
        boolean show = false;
        if (fileOriginRButton.isSelected()
            && !fileOriginTextField.getText().isEmpty()) {
                byte[] res = readFile(currentGenerateOriginFile);
                text = generate(res,currentAlgorithm);
                show = true;
                
        }
        else if((textOriginRButton.isSelected() &&
                !textOriginTextArea.getText().isEmpty())){
            text = generate(textOriginTextArea.getText().getBytes(),currentAlgorithm);
                show = true;
        }
        else{
            SimpleDialog.doError(R.getString("error"), R.getString("error_fields_empty"));
        }
        
        
        if (show) {
            if (textTargetCheckBox.isSelected()) {
                textTargetTextArea.setText(text);
            }
            else{
                textTargetTextArea.setText("");
            }
            if (fileTargetCheckBox.isSelected()) {
                if (currentGenerateTargetFile != null) {
                    writeIntoFile(text.getBytes(),currentGenerateTargetFile);
                    SimpleDialog.doInfo(R.getString("hash_generate_file_title"), R.getString("hash_generate_file_subtitle"));
                }
                else{
                    SimpleDialog.doError(R.getString("error"), R.getString("no_selected_file"));

                }
            }
            else{
                fileTargetTextField.setText("");
                currentGenerateTargetFile = null;
            }
        }
        
    }
    public void onFileOriginRadioButtonClick(ActionEvent e){
        textOriginTextArea.setVisible(false);
        chooseFileOriginButton.setVisible(true);
        fileOriginTextField.setVisible(true);
    }
    public void onTextOriginRadioButtonClick(ActionEvent e){
        textOriginTextArea.setVisible(true);
        fileOriginTextField.setVisible(false);
        chooseFileOriginButton.setVisible(false);
    }
    public void onChooseFileOriginClick(ActionEvent e){
        File file = CryptoniteSimpleFileChooser.chooseOpenFile(
                Cryptonite.getStage(),
                R.getString("choose_file_open"));
        if (file != null) {
            try {
                if (file.exists()) {
                    String path = file.getCanonicalPath();
                    fileOriginTextField.setText(path);
                    currentGenerateOriginFile = file;
                }
            } catch (IOException ex) {
                Logger.getLogger(HashViewController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    public void onChooseFileTargetClick(ActionEvent e){
        File file = CryptoniteSimpleFileChooser.chooseSaveFile(
                Cryptonite.getStage(),
                R.getString("choose_file_open"),
                CryptoniteFileExtension.dat);
        if (file != null) {
            try {
                if (!file.exists())
                    file.createNewFile();
                if (file.exists()) {
                    String path = file.getCanonicalPath();
                    fileTargetTextField.setText(path);
                    currentGenerateTargetFile = file;
                }
            } catch (IOException ex) {
                Logger.getLogger(HashViewController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    public void onValidateFileOriginRadioButtonClick(ActionEvent e){
        validateTextOriginTextArea.setVisible(false);
        validateChooseFileOriginButton.setVisible(true);
        validateFileOriginTextField.setVisible(true);
    }
    public void onValidateTextOriginRadioButtonClick(ActionEvent e){
        validateTextOriginTextArea.setVisible(true);
        validateFileOriginTextField.setVisible(false);
        validateChooseFileOriginButton.setVisible(false);
    }
    public void onValidateFileTargetRadioButtonClick(ActionEvent e){
        validateTextTargetTextArea.setVisible(false);
        validateChooseFileTargetButton.setVisible(true);
        validateFileTargetTextField.setVisible(true);
    }
    public void onValidateTextTargetRadioButtonClick(ActionEvent e){
        validateTextTargetTextArea.setVisible(true);
        validateFileTargetTextField.setVisible(false);
        validateChooseFileTargetButton.setVisible(false);
    }
    public void onValidateChooseFileOriginClick(ActionEvent e){
        File file = CryptoniteSimpleFileChooser.chooseOpenFile(
                Cryptonite.getStage(),
                R.getString("choose_file_open"));
        if (file != null) {
            try {
                if (!file.exists())
                    file.createNewFile();
                if (file.exists()) {
                    String path = file.getCanonicalPath();
                    validateFileOriginTextField.setText(path);
                    currentValidateOriginFile = file;
                }
            } catch (IOException ex) {
                Logger.getLogger(HashViewController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    public void onValidateChooseFileTargetClick(ActionEvent e){
        File file = CryptoniteSimpleFileChooser.chooseSaveFile(
                Cryptonite.getStage(),
                R.getString("choose_file_open"),
                CryptoniteFileExtension.dat);
        if (file != null) {
            try {
                if (!file.exists())
                    file.createNewFile();
                if (file.exists()) {
                    String path = file.getCanonicalPath();
                    validateFileTargetTextField.setText(path);
                    currentValidateTargetFile = file;
                }
            } catch (IOException ex) {
                Logger.getLogger(HashViewController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    public void onValidateCleanButtonClick(ActionEvent e) {
        if (anyValidateFieldFilled()){
            if (SimpleDialog.doQuestion(R.getString("warning"), R.getString("warning_fields_message"))) {
                validateFileTargetTextField.clear();
                validateTextTargetTextArea.clear();
                validateFileOriginTextField.clear();
                validateTextOriginTextArea.clear();
                currentValidateTargetFile = null;
                currentValidateOriginFile = null;
                validateInfoLabel.setVisible(false);
            }
        }
    }
    public void onValidateButtonClick(ActionEvent e) {
        if (readyToValidate()) {
            String origin = "", originHash;
            String target = "";
            // GET ORIGIN TEXT
            if (validateFileOriginRButton.isSelected()) {
                origin = new String(readFile(currentValidateOriginFile));
            }
            else if (validateTextOriginRButton.isSelected()) {
                origin = validateTextOriginTextArea.getText();
            }
            // CONVERT ORIGIN TO HASH
            HashProcess conversion = new HashProcess(validateTextOriginTextArea.getText(),currentAlgorithm);
            conversion.process();
            originHash = conversion.getMessageProcessedAsString();
            
            //GET TARGET TEXT
            if (validateFileTargetRButton.isSelected()) {
                target = new String(readFile(currentValidateTargetFile));
            }
            else if (validateTextTargetRButton.isSelected()) {
                target = validateTextTargetTextArea.getText();
            }
            
            // FINAL CHECK
            if (originHash.equals(target)) {
                validateInfoLabel.setVisible(true);
                validateInfoLabel.setText(R.getString("validate_hash_valid"));
                validateInfoLabel.setStyle("-fx-text-fill:darkgreen;");
            }
            else{
                validateInfoLabel.setVisible(true);
                validateInfoLabel.setText(R.getString("validate_hash_no_valid"));
                validateInfoLabel.setStyle("-fx-text-fill:darkred;");
            }
        }
        else{
            SimpleDialog.doError(R.getString("error"), R.getString("error_fields_empty"));
        }
    }
    public void onFileTargetCheckBoxClick(ActionEvent e){
        CheckBox checkBox = (CheckBox)e.getSource();
        if (checkBox.isSelected())  enableTargetFile();
        else                        disableTargetFile();
    }
    public void onTextTargetCheckBoxClick(ActionEvent e){
        CheckBox checkBox = (CheckBox)e.getSource();
        if (checkBox.isSelected())  enableTargetText();
        else                        disableTargetText();
    }
    public void onExitClick(MouseEvent e){
        stretchCloseTransition(this::exit);
        
    }
    public void onBackClick(MouseEvent e){
        stretchCloseTransition(this::back);
    }
    
    //ACCIONES DE CAMBIO DE STAGE
    public void exit(ActionEvent e){
        try {
            Cryptonite.closeStage();
        } catch (Exception ex) {
            Logger.getLogger(HashViewController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void back(ActionEvent e){
        try {
            Cryptonite.closeStage();
            Cryptonite.startMenuStage();
        } catch (Exception ex) {
            Logger.getLogger(HashViewController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    // UTILIDADES
    public byte[] readFile(File file){
        try {
            RandomAccessFile raf = new RandomAccessFile(file,"r");
            byte[] arr = new byte[(int)raf.length()];
            raf.readFully(arr);
            return arr;
        } catch (IOException ex) {
            Logger.getLogger(HashViewController.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    public void writeIntoFile(byte[] bytes, File file){
        try {
            if (file.exists()) {
                PrintWriter writer = new PrintWriter(file); // eliminar contenido
                writer.print("");
                writer.close();
            }
            
            RandomAccessFile raf = new RandomAccessFile(file,"rw");
            for (int i = 0; i < bytes.length; i++) {
                raf.write(bytes[i]);
            }
            
            raf.close();
        } catch (IOException ex) {
            Logger.getLogger(HashViewController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    byte[] result;
    public void onReadSuccess(byte[] read){
        textTargetTextArea.setText(new String(read));
    }
    
    public byte[] getPrimitiveByte(Byte[] bytes){
        byte[] res = new byte[bytes.length];
        System.arraycopy(bytes, 0, res, 0, bytes.length);
        return res;
    }
}
