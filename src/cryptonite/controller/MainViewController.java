/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cryptonite.controller;

import cryptonite.Cryptonite;
import cryptonite.res.Language;
import cryptonite.res.R;
import cryptonite.res.ResourceReadException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.ScaleTransition;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.StackPane;
import javafx.util.Duration;

/**
 *
 * @author Luciano
 */
public class MainViewController implements Initializable {
    
    @FXML
    private ImageView exitImage;
    @FXML
    private Button chooseButton;
    @FXML
    private AnchorPane mainPanel;
    @FXML
    private FlowPane languagePanel;
    @FXML
    private StackPane cryptoniteLogoStackPanel;
    
    @FXML
    private CheckBox defaultLanguageCheckBox;
    
    private ToggleGroup languageSelectGroup;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        initBackground();
        initExitButton();
        initChooseButton();
        initDefaultLanguageCheckbox();
        initLanguageGroups();
        initCryptoniteLogo();
        initMainPanel();
        stretchOpenTransition();
    }    
    
    // INICIALIZACIONES
    public void initChooseButton(){
        chooseButton.setText(R.getString("choose"));
        chooseButton.setOnAction(this::onChooseClick);
    }
    public void initDefaultLanguageCheckbox(){
        R.setString(defaultLanguageCheckBox, "default_language");
        if (Cryptonite.isIgnoreMainStage()) {
            defaultLanguageCheckBox.setSelected(true);
        }
    }
    public void initBackground(){
        R.getImage("background1").setAsBackgroundCover(mainPanel);
    }
    private double xOffsetTemporal;
    private double yOffsetTemporal;
    public void initMainPanel(){ // se puede mover y arrastrar
        mainPanel.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                xOffsetTemporal = Cryptonite.getStage().getX() - event.getScreenX();
                yOffsetTemporal = Cryptonite.getStage().getY() - event.getScreenY();
            }
        });
        mainPanel.setOnMouseDragged(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                Cryptonite.getStage().setX(event.getScreenX() + xOffsetTemporal);
                Cryptonite.getStage().setY(event.getScreenY() + yOffsetTemporal);
            }
        });
    }
    public void initCryptoniteLogo(){
        R.getImage("textlogo_subtitle").setAsBackgroundContained(cryptoniteLogoStackPanel);
    }
    public void initExitButton(){
        R.getImage("cross").setAsImage(exitImage);
        exitImage.setOnMouseClicked(this::onExitClick);
    }
    public void initLanguageGroups(){
        languageSelectGroup = new ToggleGroup();
        for (Language lang : Language.values()) {
            RadioButton curr = new RadioButton();
            curr.setId(lang.getIsoCode6391());
            curr.setText(lang.getName());
            curr.getStyleClass().add("whiteText");
            curr.getStyleClass().add("boldText");
            if (R.getCurrentLanguage().equals(lang)) {
                curr.setSelected(true);
            }
            curr.setOnAction(this::onLanguageRadioButtonClick);
            languagePanel.getChildren().add(curr);
            languageSelectGroup.getToggles().add(curr);
        }
    }
    
    // ON CLICK LISTENERS
    public void onExitClick(MouseEvent event){
        stretchCloseTransition(this::exit);
    }
    public void onChooseClick(ActionEvent ev){
        stretchCloseTransition(this::continueStageFinish);
    }
    public void onLanguageRadioButtonClick(ActionEvent event){
        //
        RadioButton source = (RadioButton)event.getSource();
        for (Language value : Language.values()) {
            if (source.getId().equals(value.getIsoCode6391()))
                changeLanguage(value);
        }
    }
    
    // ACCIONES
    public void changeLanguage(Language target){
        try {
            R.setLanguage(target);
            initExitButton();
            initChooseButton();
            initDefaultLanguageCheckbox();
        } catch (ResourceReadException ex) {
            Logger.getLogger(MainViewController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    // TRANSICIONES ABRIR-CERRAR ESCENARIO
    public void stretchOpenTransition(){
        ScaleTransition transition = new ScaleTransition();
        mainPanel.setScaleX(0);
        transition.setNode(mainPanel);
        transition.setDuration(Duration.seconds(0.1));
        transition.setToX(1);
        transition.play();
    }
    public void stretchCloseTransition(EventHandler<ActionEvent> event){
        ScaleTransition transition = new ScaleTransition();
        transition.setNode(mainPanel);
        transition.setDuration(Duration.seconds(0.1));
        transition.setToX(0);
        transition.play();
        transition.setOnFinished(event);
    }
    
    // CAMBIO ESCENARIO
    public void exit(ActionEvent ev){
        Cryptonite.closeStage();
    }
    public void continueStageFinish(ActionEvent ev){
        try {
            String lang = ((RadioButton)languageSelectGroup.getSelectedToggle()).getId();
            if (defaultLanguageCheckBox.isSelected()) {
                Cryptonite.writeDefaultLanguage(Language.findByIsoCode6391(lang));
            }
            Cryptonite.closeStage();
            Cryptonite.startMenuStage();
        } catch (Exception ex) {
            Logger.getLogger(MainViewController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
