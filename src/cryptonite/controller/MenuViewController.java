/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cryptonite.controller;

import cryptonite.Cryptonite;
import cryptonite.res.R;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.ScaleTransition;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.util.Duration;

/**
 *
 * @author Luciano
 */
public class MenuViewController implements Initializable {

    private @FXML AnchorPane mainPanel;
    private @FXML StackPane logoPanel;

    private @FXML ImageView imageExit;
    private @FXML ImageView imageLanguage;
    
    private @FXML Label createdByLabel;
    
    private @FXML Button hashButton;
    private @FXML Button cryptoButton;
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        initBackground();
        initImageExit();
        initImageLanguage();
        initLogoPanel();
        initVersionLabel();
        initHashViewButton();
        initCryptoViewButton();
        stretchOpenTransition();
    }
    
    //INICIALIZACIONES PARA DAR VALOR A CADA PARTE
    public void initVersionLabel(){
        String text = 
                R.getDataString("app_fullname")+" "+R.getDataString("version")+"\n"+
                R.getString("created_by")+" "+
                    R.getDataString("developer1")+" "+
                        R.getString("and")+" "+
                    R.getDataString("developer2");
        createdByLabel.setText(text);
    }
    public void initBackground() {
        R.getImage("background_menu").setAsBackgroundCover(mainPanel);
    }
    public void initImageExit(){
        R.getImage("cross").setAsImage(imageExit);
        imageExit.setOnMouseClicked(this::onExitClick);
    }
    public void initImageLanguage(){
        R.getImage("language").setAsImage(imageLanguage);
        imageLanguage.setOnMouseClicked(this::onLangClick);
    }
    private double xOffsetTemporal;
    private double yOffsetTemporal;
    public void initLogoPanel(){
        R.getImage("textlogo_title").setAsBackgroundContained(logoPanel);
        logoPanel.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                xOffsetTemporal = Cryptonite.getStage().getX() - event.getScreenX();
                yOffsetTemporal = Cryptonite.getStage().getY() - event.getScreenY();
            }
        });
        logoPanel.setOnMouseDragged(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                Cryptonite.getStage().setX(event.getScreenX() + xOffsetTemporal);
                Cryptonite.getStage().setY(event.getScreenY() + yOffsetTemporal);
            }
        });
    }
    public void initHashViewButton() {
        hashButton.setText(R.getString("hash_tools"));
        Image image = R.getImage("hash").getAsImage();
        ImageView view = new ImageView(image);
        view.setPreserveRatio(true);
        view.setFitHeight(150);
        hashButton.setGraphic(view);
        hashButton.setOnAction(this::onHashButtonClick);
    }
    public void initCryptoViewButton() {
        cryptoButton.setText(R.getString("crypto_tools"));
        Image image = R.getImage("key").getAsImage();
        ImageView view = new ImageView(image);
        view.setPreserveRatio(true);
        view.setFitHeight(150);
        cryptoButton.setGraphic(view);
        cryptoButton.setOnAction(this::onCryptoButtonClick);
    }
    
    //ACCIONES CLICK
    public void onLangClick(MouseEvent e){
        stretchCloseTransition(this::openLanguageView);
    }
    public void onExitClick(MouseEvent e){
        stretchCloseTransition(this::exit);
    }
    public void onHashButtonClick(ActionEvent e){
        stretchCloseTransition(this::openHashView);
    }
    public void onCryptoButtonClick(ActionEvent e){
        stretchCloseTransition(this::openCryptoView);
    }
    
    // TRANSICIONES ABRIR-CERRAR ESCENARIO
    public void stretchOpenTransition(){
        ScaleTransition transition = new ScaleTransition();
        mainPanel.setScaleX(0);
        transition.setNode(mainPanel);
        transition.setDuration(Duration.seconds(0.1));
        transition.setToX(1);
        transition.play();
    }
    public void stretchCloseTransition(EventHandler<ActionEvent> ev){
        ScaleTransition transition = new ScaleTransition();
        transition.setNode(mainPanel);
        transition.setDuration(Duration.seconds(0.1));
        transition.setToX(0);
        transition.play();
        transition.setOnFinished(ev);
    }
    
    // CAMBIOS DE ESCENARIO
    public void exit(ActionEvent e){
        Cryptonite.closeStage();
    }
    public void openLanguageView(ActionEvent e){
        try {
            Cryptonite.closeStage();
            Cryptonite.startMainStage();
        } catch (Exception ex) {
            Logger.getLogger(MenuViewController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void openHashView(ActionEvent e){
        try {
            Cryptonite.closeStage();
            Cryptonite.startHashStage();
        } catch (Exception ex) {
            Logger.getLogger(MenuViewController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void openCryptoView(ActionEvent e){
        try {
            Cryptonite.closeStage();
            Cryptonite.startCryptoStage();
        } catch (Exception ex) {
            Logger.getLogger(MenuViewController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
