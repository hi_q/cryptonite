/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cryptonite.controller.utils;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

/**
 *
 * @author Luciano
 */
public class CryptoniteSimpleFileChooser {

    public static File chooseOpenFile(Stage toFreeze, String title) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle(title);
        return fileChooser.showOpenDialog(toFreeze);
    }

    public static File chooseSaveFile(Stage toFreeze, String title, CryptoniteFileExtension ext) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle(title);
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter(ext.getText(), ext.getExtension());
        fileChooser.getExtensionFilters().add(extFilter);
        File result = fileChooser.showSaveDialog(toFreeze);
        return result;
    }
}
