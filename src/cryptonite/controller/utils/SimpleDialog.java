/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cryptonite.controller.utils;

import java.util.Optional;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;

/**
 *
 * @author Luciano
 */
public class SimpleDialog {
    public static boolean doQuestion(String title, String message){
        Alert alert = 
        new Alert(Alert.AlertType.WARNING, 
            message,
             ButtonType.OK, 
             ButtonType.CANCEL);
            alert.setTitle(title);
            Optional<ButtonType> result = alert.showAndWait();

            return result.get() == ButtonType.OK;
    }
    public static void doError(String title, String message){
        Alert alert = 
        new Alert(Alert.AlertType.ERROR, 
            message,
             ButtonType.OK);
            alert.setTitle(title);
            Optional<ButtonType> result = alert.showAndWait();
    }
    public static void doInfo(String title, String message){
        Alert alert = 
        new Alert(Alert.AlertType.INFORMATION, 
            message,
             ButtonType.OK);
            alert.setTitle(title);
            Optional<ButtonType> result = alert.showAndWait();
    }
}
