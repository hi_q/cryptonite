/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cryptonite.controller.utils;

import cryptonite.res.R;

/**
 *
 * @author Luciano
 */
public enum CryptoniteFileExtension {
    ANY(String.format("%s (*.%s)",
            R.getString("any_files"),
            "*")                    ,"*.*"),
    dat(String.format("%s (*.%s)",
            R.getString("hash_files"),
            "dat")                  ,"*.*"),
    aes(String.format("%s (*.%s)",
            R.getString("encrypted_files"),
            "aes")                  ,"*.*");
    
    private String text;
    private String extension;

    private CryptoniteFileExtension(String text, String extension) {
        this.text = text;
        this.extension = extension;
    }
    
    public String getText() {
        return text;
    }

    public String getExtension() {
        return extension;
    }
    
    
}
