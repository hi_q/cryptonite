/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cryptonite.controller;

import cryptonite.Cryptonite;
import cryptonite.controller.utils.CryptoniteFileExtension;
import cryptonite.controller.utils.CryptoniteSimpleFileChooser;
import cryptonite.controller.utils.SimpleDialog;
import cryptonite.res.R;
import cryptonite.utils.cryptoutils.CryptoAESProcess;
import cryptonite.utils.cryptoutils.CryptoSymmetricAlgorithm;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.ScaleTransition;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.util.Duration;

/**
 *
 * @author Luciano
 */
public class CryptoViewController implements Initializable {
    
    // MAIN PANEL (toda la vista entera esta dentro de esto
    private @FXML AnchorPane mainPanel;
    
    // SECCION BANNER
    private @FXML AnchorPane bannerPanel;
    private @FXML ImageView backImage;
    private @FXML ImageView exitImage;
    
    //SECCION ALGORITMOS
    private @FXML Label algorithmLabel;
    private CryptoSymmetricAlgorithm algorithm = CryptoSymmetricAlgorithm.AES;
    
    //SECCION ENCRIPTAR
        //label
        private @FXML Label encryptLabelTitle;
        private @FXML Label encryptLabelName;
        private @FXML Label encryptLabelPassword;
        private @FXML Label encryptLabelConfirmPassword;
        private @FXML Label encryptLabelTarget;
        private @FXML Label encryptLabelInfo;
        //texfields
        private @FXML TextField encryptFileOriginTextField;
        private @FXML TextField encryptPasswordField;
        private @FXML TextField encryptConfirmPasswordField;
        private @FXML TextField encryptFileTargetTextField;
        //buttons
        private @FXML Button encryptChooseOriginFile;
        private @FXML Button encryptChooseTargetFile;
        private File encryptOriginFile = null;
        private File encryptTargetFile = null;
        private @FXML Button encryptButton;
        private @FXML Button encryptCleanButton;
        //others
        private @FXML CheckBox encryptCheckBoxDelete;

     //SECCION DESENCRIPTAR
          //labels
        private @FXML Label decryptLabelTitle;
        private @FXML Label decryptLabelName;
        private @FXML Label decryptLabelPassword;
        private @FXML Label decryptLabelTarget;
        private @FXML Label decryptLabelInfo;
        //textfields
        private @FXML TextField decryptFileOriginTextField;    
        private @FXML PasswordField decryptPasswordField;
        private @FXML TextField decryptFileTargetTextField;
        //buttons
        private @FXML Button decryptChooseOriginFile;
        private @FXML Button decryptButton;
        private @FXML Button decryptCleanButton;
        private @FXML Button decryptChooseTargetFile;
        private File decryptOriginFile;
        private File decryptTargetFile;
        //others
        private @FXML CheckBox decryptCheckBoxDelete;
        
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        initBanner();
        initBackImage();
        initExitImage();
        initAlgorithmSection();
        initEncryptSection();
        initDecryptSection();
        stretchOpenTransition();
    }
    
    // FASES DE LA INICIALIZACION
    private double xOffsetTemporal;
    private double yOffsetTemporal;
    public void initBanner(){ // banner se mueve
        R.getImage("banner").setAsBackgroundCover(bannerPanel);
        bannerPanel.getStyleClass().add("posDown");
        
        // METODOS ARRASTRAR VENTANA
        bannerPanel.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                xOffsetTemporal = Cryptonite.getStage().getX() - event.getScreenX();
                yOffsetTemporal = Cryptonite.getStage().getY() - event.getScreenY();
            }
        });
        bannerPanel.setOnMouseDragged(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                Cryptonite.getStage().setX(event.getScreenX() + xOffsetTemporal);
                Cryptonite.getStage().setY(event.getScreenY() + yOffsetTemporal);
            }
        });
    }
    public void initBackImage(){
        R.getImage("back").setAsImage(backImage);
        backImage.setOnMouseClicked(this::onBackClick);
    }
    public void initExitImage(){
        R.getImage("cross").setAsImage(exitImage);
        exitImage.setOnMouseClicked(this::onExitClick);
    }
    public void initAlgorithmSection(){
        initAlgorithmLabel();
    }
    public void initAlgorithmLabel(){
            algorithmLabel.setText(String.format(R.getString("using_algorithm"), algorithm.getLongName()));
        }
    //Michael
    public void initEncryptSection(){
        initEncryptLabel();
        initEncryptTextContent();
        initEncryptButtons();
        initEncryptChooseButtons();
        initEncryptDeleteCheckBox();
    }
        public void initEncryptLabel(){
            encryptLabelTitle.setText(R.getString("encrypt_title"));
            encryptLabelInfo.setVisible(false);
        }
        public void initEncryptTextContent(){ 
            encryptLabelName.setText(R.getString("choose_file"));
            encryptLabelPassword.setText(R.getString("select_password"));
            encryptLabelConfirmPassword.setText(R.getString("confirm_password"));
            encryptLabelTarget.setText(R.getString("select_target"));
            encryptLabelInfo.setText(R.getString("encrypt_info"));
            encryptButton.setText(R.getString("encrypt_button"));
            encryptCheckBoxDelete.setText(R.getString("encrypt_checkbox"));
            encryptCleanButton.setText(R.getString("clean"));
        }
        public void initEncryptButtons(){
            R.getImage("logo").setAsGraphic(encryptButton);
            encryptButton.setOnAction(this::onEncryptClick);
            encryptCleanButton.setOnAction(this::onEncryptCleanClick);
        }
        
        public void initEncryptChooseButtons(){
            R.getImage("find").setAsBackgroundContained(encryptChooseOriginFile);
            R.getImage("find").setAsBackgroundContained(encryptChooseTargetFile);
            encryptChooseOriginFile.setOnAction(this::onEncryptChooseOriginFileClick);
            encryptChooseTargetFile.setOnAction(this::onEncryptChooseTargetFileClick);
        }
        public void initEncryptDeleteCheckBox(){
            // encryptCheckBoxDelete.setOnAction(.........);  // ACCIONES
        }  
    public void initDecryptSection(){
        initDecryptLabel();
        initDecryptTextContent();
        initDecryptButtons();
        initDecryptChooseButtons();
        initDecryptDeleteCheckBox();
    }
        public void initDecryptLabel(){
            decryptLabelTitle.setText(R.getString("decrypt_title"));
            decryptLabelInfo.setVisible(false);
        }
        public void initDecryptTextContent(){ 
            decryptLabelName.setText(R.getString("choose_file"));
            decryptLabelPassword.setText(R.getString("select_password"));
            decryptLabelTarget.setText(R.getString("select_target"));
            decryptLabelInfo.setText(R.getString("decrypt_info"));
            decryptButton.setText(R.getString("decrypt_button"));
            decryptCheckBoxDelete.setText(R.getString("decrypt_checkbox"));
            decryptCleanButton.setText(R.getString("clean"));
        }
        public void initDecryptButtons(){
            R.getImage("logo_open").setAsGraphic(decryptButton);
            decryptButton.setOnAction(this::onDecryptClick);
            decryptCleanButton.setOnAction(this::onDecryptCleanClick);
        }
        public void initDecryptChooseButtons() {
            R.getImage("find").setAsBackgroundContained(decryptChooseOriginFile);
            R.getImage("find").setAsBackgroundContained(decryptChooseTargetFile);
            decryptChooseOriginFile.setOnAction(this::onDecryptChooseOriginFileClick);
            decryptChooseTargetFile.setOnAction(this::onDecryptChooseTargetFileClick);
        }
        public void initDecryptDeleteCheckBox(){
            
        }  
    
    // COMPROBACIONES
    public boolean readyToEncrypt() {
        boolean result = true;
        boolean fieldsEmpty = false;
        boolean errorPass = false;
        boolean fileNotFound = false;
        if (encryptOriginFile != null) {
            if (!encryptOriginFile.exists()) {
                fileNotFound = true;
                result = false;
            }
        }
        else{
            fieldsEmpty = true;
            result = false;
        }
        if (encryptTargetFile == null) {
            fieldsEmpty = true;
            result = false;
        }
        if (!encryptPasswordField.getText().equals
            (encryptConfirmPasswordField.getText())) {
            errorPass = true;
            result =  false;
        }
        String currentError = "";
        if (fieldsEmpty) {
            currentError += R.getString("error_fields_empty")+"\n";
        }
        if (fileNotFound) {
            currentError += R.getString("error_file_exists")+"\n";
        }
        if (errorPass) {
            currentError += R.getString("error_match_password")+"\n";
        }
        currentError = currentError.trim();
        if (!result) {
            alertError(R.getString("error"),currentError);
        }
        return result;
    }
    public boolean readyToDecrypt() {
        boolean result = true;
        boolean fieldsEmpty = false;
        boolean fileNotFound = false;
        if (decryptOriginFile != null) {
            if (!decryptOriginFile.exists()) {
                fileNotFound = true;
                result = false;
            }
        }
        else{
            fieldsEmpty = true;
            result = false;
        }
        if (decryptTargetFile == null) {
            fieldsEmpty = true;
            result = false;
        }
        if (decryptPasswordField.getText().isEmpty()) {
            fieldsEmpty = true;
            result =  false;
        }
        String currentError = "";
        if (fieldsEmpty) {
            currentError += R.getString("error_fields_empty")+"\n";
        }
        if (fileNotFound) {
            currentError += R.getString("error_file_exists")+"\n";
        }
        currentError = currentError.trim();
        if (!result) {
            alertError(R.getString("error"),currentError);
        }
        return result;
    }
    public boolean anyEncryptValue() {
        if (encryptOriginFile != null) return true;
        if (encryptTargetFile != null) return true;
        if (!encryptFileOriginTextField.getText().isEmpty()) return true;
        if (!encryptFileTargetTextField.getText().isEmpty()) return true;
        if (!encryptPasswordField.getText().isEmpty()) return true;
        if (!encryptConfirmPasswordField.getText().isEmpty()) return true;
        if (encryptCheckBoxDelete.isSelected()) return true;
        
        return false;
    }
    public boolean anyDecryptValue() {
        if (decryptOriginFile != null) return true;
        if (decryptTargetFile != null) return true;
        if (!decryptFileOriginTextField.getText().isEmpty()) return true;
        if (!decryptFileTargetTextField.getText().isEmpty()) return true;
        if (!decryptPasswordField.getText().isEmpty()) return true;
        if (decryptCheckBoxDelete.isSelected()) return true;
        
        return false;
    }
        
    // ACCIONES SEPARADAS
    public void setNewAlgorithm(String name){ // cambiar de algoritmo
        // futura implementacion en CryptoSymmetricAlgorithm :)
    }
    public void cleanAlgorithmicDependentFields() { // limpieza en caso de cambiar de algoritmo
        // lo que haya que limpiar en caso de clicar el radio button de algoritmo
    }
    public File setOpenFileFor(String title, TextField fill) {
        File file = CryptoniteSimpleFileChooser.chooseOpenFile(Cryptonite.getStage(), title);
        if (file != null) {
            try {
                fill.setText(file.getCanonicalPath());
                return file;
            } catch (IOException ex) {
                Logger.getLogger(CryptoViewController.class.getName()).log(Level.SEVERE, null, ex);
                return null;
            }
        }
        else{
            fill.setText("");
            return null;
        }
    }
    public File setSaveFileFor(String title, TextField fill, CryptoniteFileExtension ext) {
        File file = CryptoniteSimpleFileChooser.chooseSaveFile(Cryptonite.getStage(), title, ext);
        if (file != null) {
            try {
                fill.setText(file.getCanonicalPath());
                return file;
            } catch (IOException ex) {
                Logger.getLogger(CryptoViewController.class.getName()).log(Level.SEVERE, null, ex);
                return null;
            }
        }
        else{
            fill.setText("");
            return null;
        }
    }
    public void alertError(String title, String msg) {
        SimpleDialog.doError(title, msg);
    }
    public void alertInfo(String title, String msg) {
        SimpleDialog.doInfo(title, msg);
    }
    
    // TRANSICIONES ABRIR-CERRAR ESCENARIO
    public void stretchOpenTransition(){
        ScaleTransition transition = new ScaleTransition();
        mainPanel.setScaleX(0);
        transition.setNode(mainPanel);
        transition.setDuration(Duration.seconds(0.1));
        transition.setToX(1);
        transition.play();
    }
    public void stretchCloseTransition(EventHandler<ActionEvent> ev){
        ScaleTransition transition = new ScaleTransition();
        transition.setNode(mainPanel);
        transition.setDuration(Duration.seconds(0.1));
        transition.setToX(0);
        transition.play();
        transition.setOnFinished(ev);
    }
    
    //ACCIONES LISTENER
    public void onEncryptChooseOriginFileClick(ActionEvent e) {
        encryptOriginFile = setOpenFileFor(R.getString("choose_file"),encryptFileOriginTextField);
    }
    public void onEncryptChooseTargetFileClick(ActionEvent e) {
        encryptTargetFile = setSaveFileFor(R.getString("choose_file"),encryptFileTargetTextField,CryptoniteFileExtension.aes);
    }
    public void onDecryptChooseOriginFileClick(ActionEvent e) {
        decryptOriginFile = setOpenFileFor(R.getString("choose_file"),decryptFileOriginTextField);
    }
    public void onDecryptChooseTargetFileClick(ActionEvent e) {
        decryptTargetFile = setSaveFileFor(R.getString("choose_file"),decryptFileTargetTextField,CryptoniteFileExtension.ANY);
    }
    public void onEncryptClick(ActionEvent e) {
        if (readyToEncrypt()) {
            try {
                File origin = encryptOriginFile;
                File target = encryptTargetFile;
                String pass = encryptPasswordField.getText();
                
                CryptoAESProcess process = new CryptoAESProcess(readFileIntoBytes(origin));
                process.encrypt(pass);
                writeBytesIntoFile(target,process.getMessageProcessed());
                
                if (encryptCheckBoxDelete.isSelected()) {
                    origin.delete();
                }
                
                encryptLabelInfo.setVisible(true);
                encryptLabelInfo.setStyle("-fx-text-fill: darkgreen");
                encryptLabelInfo.setText(R.getString("encrypt_successful"));
                
            } catch (IOException ex) {
                Logger.getLogger(CryptoViewController.class.getName()).log(Level.SEVERE, null, ex);
                encryptLabelInfo.setVisible(true);
                encryptLabelInfo.setStyle("-fx-text-fill: darkgreen");
                encryptLabelInfo.setText(R.getString("encrypt_error"));
            }
        }
    }
    public void onEncryptCleanClick(ActionEvent e) {
        if (anyEncryptValue()) {
            boolean action = SimpleDialog.doQuestion(R.getString("warning"), R.getString("warning_fields_message"));
            if (action) {
                encryptOriginFile = null;
                encryptTargetFile = null;
                encryptFileOriginTextField.clear();
                encryptFileTargetTextField.clear();
                encryptPasswordField.clear();
                encryptConfirmPasswordField.clear();
                encryptCheckBoxDelete.setSelected(false);
                encryptLabelInfo.setVisible(false);
            }
        }
    }
    public void onDecryptClick(ActionEvent e) {
        if (readyToDecrypt()) {
            try {
                File origin = decryptOriginFile;
                File target = decryptTargetFile;
                String pass = decryptPasswordField.getText();
                
                CryptoAESProcess process = new CryptoAESProcess(readFileIntoBytes(origin));
                process.decrypt(pass);
                writeBytesIntoFile(target,process.getMessageProcessed());
                
                if (decryptCheckBoxDelete.isSelected()) {
                    origin.delete();
                }
                
                decryptLabelInfo.setVisible(true);
                decryptLabelInfo.setStyle("-fx-text-fill: darkgreen");
                decryptLabelInfo.setText(R.getString("decrypt_successful"));
                
            } catch (IOException ex) {
                Logger.getLogger(CryptoViewController.class.getName()).log(Level.SEVERE, null, ex);
                decryptLabelInfo.setVisible(true);
                decryptLabelInfo.setStyle("-fx-text-fill: darkgreen");
                decryptLabelInfo.setText(R.getString("decrypt_error"));
            }
        }
    }
    public void onDecryptCleanClick(ActionEvent e) {
        if (anyDecryptValue()) {
            boolean action = SimpleDialog.doQuestion(R.getString("warning"), R.getString("warning_fields_message"));
            if (action) {
                decryptOriginFile = null;
                decryptTargetFile = null;
                decryptFileOriginTextField.clear();
                decryptFileTargetTextField.clear();
                decryptPasswordField.clear();
                decryptCheckBoxDelete.setSelected(false);
                decryptLabelInfo.setVisible(false);
            }
        }
    }
    public void onExitClick(MouseEvent e){
        stretchCloseTransition(this::exit);
        
    }
    public void onBackClick(MouseEvent e){
        stretchCloseTransition(this::back);
    }
    
    public void exit(ActionEvent e){
        try {
            Cryptonite.closeStage();
        } catch (Exception ex) {
            Logger.getLogger(HashViewController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void back(ActionEvent e){
        try {
            Cryptonite.closeStage();
            Cryptonite.startMenuStage();
        } catch (Exception ex) {
            Logger.getLogger(HashViewController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public byte[] readFileIntoBytes(File file) throws IOException{
        if (file.exists()) {
            RandomAccessFile raf = null;
            try {
                raf = new RandomAccessFile(file,"r");
                byte[] bytes = new byte[(int)raf.length()];
                for (int i = 0; i < raf.length(); i++) {
                    bytes[i] = raf.readByte();
                }
                return bytes;
            } catch (FileNotFoundException ex) {
                System.err.println(ex);
                return null;
            }
            finally{
                if (raf != null) raf.close();
            }
        }
        else{
            return new byte[0];
        }
    }
    public void writeBytesIntoFile(File file, byte[] bytes) throws IOException{
        if (file.exists()) {
            RandomAccessFile raf = null;
            try {
                raf = new RandomAccessFile(file,"rw");
                if (raf.length() != 0) {
                    raf.setLength(0);
                }
                for (int i = 0; i < bytes.length; i++) {
                    if (bytes[i] != 0) { // si byte no es nulo
                        raf.writeByte(bytes[i]);
                    }
                }
            } catch (FileNotFoundException ex) {
                System.err.println(ex);
            }
            finally{
                if (raf != null) raf.close();
            }
        }
        else{
            file.createNewFile();
            writeBytesIntoFile(file,bytes);
        }
    }
    
}
