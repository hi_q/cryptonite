/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cryptonite.res;

import java.io.File;
import javafx.scene.Node;
import javafx.scene.control.Labeled;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 *
 * @author Luciano
 */
public class ImageResource extends Resource {
    private static final String BACKGROUND_STYLE_FORMAT = "-fx-background-image: url('file:%s');";
    private static final String CENTERED =  "-fx-background-position: center center; ";
    private static final String NO_REPEAT =  "-fx-background-repeat: no-repeat; ";
    private static final String CONTAINED = "-fx-background-size: contain; ";
    private static final String COVER = "-fx-background-size: cover; ";
    public ImageResource(String key, String path) {
        super(key, path); 
    }
    public Image getAsImage(){
        Image temp  =new Image(new File(super.getLocation()).toURI().toString());
        double width = temp.getWidth();
        double height = temp.getHeight();
        return new Image(new File(super.getLocation()).toURI().toString(),width,height,true,false);
    }
    public ImageView getAsImageView(){
        ImageView imageView = new ImageView(getAsImage());
        imageView.setId(getKey());
        imageView.setPreserveRatio(true);
        imageView.getStyleClass().add("imageResource"); // clase css para futuras manipulaciones
        imageView.setSmooth(false);
        return imageView;
    }
    public void setAsBackground(Node node){
        node.setStyle(String.format(BACKGROUND_STYLE_FORMAT,this.getLocation()));
    }
    public void setAsBackgroundCover(Node node){
        setAsBackground(node);
        node.setStyle(node.getStyle() + COVER + NO_REPEAT + CENTERED);
    }
    public void setAsBackgroundContained(Node node){
        setAsBackground(node);
        node.setStyle(node.getStyle() + CONTAINED + NO_REPEAT + CENTERED);
    }
    public void setAsImage(ImageView view){
        double width = view.getFitWidth();
        double height = view.getFitHeight();
        Image image = new Image(new File(super.getLocation()).toURI().toString(),width,height,true,false);
        view.setImage(image);
    }
    public void setAsGraphic(Labeled node){
        double width = node.getPrefWidth();
        double height = node.getPrefHeight();
        Image img = new Image(new File(super.getLocation()).toURI().toString(),width,height,true,false);
        ImageView put = new ImageView(img);
        put.setPreserveRatio(true);
        put.setFitWidth(width-5);
        put.setFitHeight(height-5);
        node.setGraphic(put);
    }
}
