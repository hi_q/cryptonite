/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cryptonite.res;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.control.Labeled;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author Luciano
 */
public class R {
    private static HashMap<String,String> string;
    private static HashMap<String,String> data; // strings fijos, sin idioma
    private static HashMap<String,ImageResource> images; // strings fijos, sin idioma
    private static Language currentLanguage;
    
    private final static String STARTED_PROC = "Started";
    private final static String FINISHED_PROC = "Finished";
    private final static String FAILED_PROC = "Failed";
    private final static String LOAD_DATA_MESSAGE = "%s loading data resources: %s\n";
    private final static String LOAD_IMAGE_MESSAGE = "%s loading image resources: %s\n";
    private final static String LOAD_STRINGS_MESSAGE = "%s loading string resources for language %s (%s,%s): %s\n";
    private final static String FILE_NOT_FOUND = "File not found.";
    private final static String FILE_WRONG_FORMAT = "Not xml format in file.";
    private final static String FILE_WRONG_STRUCTURE = "Bad xml structure format in file.";
    private final static String WARNING_BAD_IMAGE_PATH = "WARNING while reading IMAGE resources file. Path not found (%s).\n";
    private final static String WARNING_BAD_TAG = "WARNING while reading resources file. Unexpected tag (%s).\n";
    private final static String RESOURCE_READ = "Resource loaded:\t"
                                                + "type(\"%s\"), "
                                                + "key(\"%s\"), "
                                                + "value(\"%s\");\n";
    
    private final static String LOCATION_XML_DATA_FORMAT = "src/cryptonite/res/xml/data.xml";
    private final static String LOCATION_XML_IMAGE_FORMAT = "src/cryptonite/res/xml/images.xml";
    private final static String LOCATION_XML_STRINGS_FORMAT = "src/cryptonite/res/xml/strings/strings_%s.xml";
    private final static String RESOURCES_ROOT = "resources";
    private final static String KEY_ATTR = "key";
    private final static String STRINGS_TAG = "string";
    private final static String IMAGES_TAG = "image";
    
    private final static Language DEFAULT_LANGUAGE = Language.English;
    private final static String DEFAULT_STRING = "N/A";
    private final static ImageResource DEFAULT_IMAGE = new ImageResource("DEFAULT","default-image.png");
    
    static{
        try{
            System.out.println("R initialization. Started loading resources.");
            System.out.println("");
            setLanguage(DEFAULT_LANGUAGE);
            System.out.println("");
            loadData();
            System.out.println("");
            loadImages();
            System.out.println("");
            
            System.out.println("R is ready to work. Finished loading resources.");
            System.out.println("");
        } catch (ResourceReadException ex) {
            Logger.getLogger(R.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private static void cleanStrings(){
        string = new HashMap<>();
    }
    private static void cleanData(){
        data = new HashMap<>();
    }
    private static void cleanImages(){
        images = new HashMap<>();
    }
    private static void loadStrings(Language language) throws ResourceReadException {
        File file = new File(String.format(LOCATION_XML_STRINGS_FORMAT,language.getIsoCode6391()));
        if (file.exists()) { // empieza
            System.out.printf(LOAD_STRINGS_MESSAGE,
                    STARTED_PROC,
                    language.getName(),
                    language.getIsoCode6391(),
                    language.getIsoCode6392(),
                    "");
            currentLanguage = language;
            cleanStrings();
            readStringFile(file);
            System.out.printf(LOAD_STRINGS_MESSAGE,
                    FINISHED_PROC,
                    language.getName(),
                    language.getIsoCode6391(),
                    language.getIsoCode6392(),
                    "OK.");
        }
        else{ // archivo strings no existe
            currentLanguage = null;
            throw new ResourceReadException(String.format(LOAD_STRINGS_MESSAGE,
                    FAILED_PROC,
                    language.getName(),
                    language.getIsoCode6391(),
                    language.getIsoCode6392(),
                    FILE_NOT_FOUND));
        }
    }
    private static void loadImages() throws ResourceReadException{
        File file = new File(String.format(LOCATION_XML_IMAGE_FORMAT));
        if (file.exists()) { // empieza
            System.out.printf(
                    LOAD_IMAGE_MESSAGE,
                    STARTED_PROC,
                    "");
            cleanImages();
            readImagesFile(file);
            System.out.printf(
                    LOAD_IMAGE_MESSAGE,
                    FINISHED_PROC,
                    "OK.");
        }
        else{ // archivo strings no existe
            currentLanguage = null;
            throw new ResourceReadException(String.format(
                    LOAD_IMAGE_MESSAGE,
                    FAILED_PROC,
                    FILE_NOT_FOUND));
        }
    }
    private static void loadData() throws ResourceReadException{
        File file = new File(String.format(LOCATION_XML_DATA_FORMAT));
        if (file.exists()) { // empieza
            System.out.printf(
                    LOAD_DATA_MESSAGE,
                    STARTED_PROC,
                    "");
            cleanData();
            readDataFile(file);
            System.out.printf(
                    LOAD_DATA_MESSAGE,
                    FINISHED_PROC,
                    "OK.");
        }
        else{ // archivo strings no existe
            currentLanguage = null;
            throw new ResourceReadException(String.format(
                    LOAD_DATA_MESSAGE,
                    FAILED_PROC,
                    FILE_NOT_FOUND));
        }
    }
    private static void readImagesFile(File toRead) throws ResourceReadException{
        Document doc = null;
        try {
            doc = readXmlDocument(toRead);
        } catch (Exception ex) {
            throw new ResourceReadException(String.format(
                    LOAD_IMAGE_MESSAGE,
                    FAILED_PROC,
                    FILE_WRONG_FORMAT));
        }
        HashMap<String,String> maps;
        maps = parseResourcesDocument(doc);
        if (maps != null) {
            HashMap<String,ImageResource> convertedPathMapToImage = imagePathMapToResource(maps);
            images = convertedPathMapToImage;
        }
        else throw new ResourceReadException(String.format(LOAD_IMAGE_MESSAGE,
                        FAILED_PROC,
                        FILE_WRONG_STRUCTURE));
    }
    private static HashMap<String,ImageResource> imagePathMapToResource(HashMap<String,String> pathMap){
        HashMap<String,ImageResource> imageMap = new HashMap<String,ImageResource>();
        for (Map.Entry<String,String> entry : pathMap.entrySet()) {
            String key = entry.getKey();
            String val = entry.getValue();
            if (new File(val).exists()) {
                imageMap.put(key, new ImageResource(key,val));
            }
            else{
                System.err.printf(WARNING_BAD_IMAGE_PATH,val);
            }
        }
        return imageMap;
    }
    private static void readDataFile(File toRead) throws ResourceReadException{
        Document doc = null;
        try {
            doc = readXmlDocument(toRead);
        } catch (Exception ex) {
            throw new ResourceReadException(String.format(
                    LOAD_DATA_MESSAGE,
                    FAILED_PROC,
                    FILE_WRONG_FORMAT));
        }
        
        HashMap<String,String> data;
        data = parseResourcesDocument(doc);
        if (data != null) {
            R.data = data;
        }
        else throw new ResourceReadException(String.format(LOAD_DATA_MESSAGE,
                        FAILED_PROC,
                        FILE_WRONG_STRUCTURE));
    }
    private static void readStringFile(File toRead) throws ResourceReadException{
        Document doc = null;
        try {
            doc = readXmlDocument(toRead);
        } catch (Exception ex) {
            throw new ResourceReadException(String.format(LOAD_STRINGS_MESSAGE,
                        FAILED_PROC,
                        currentLanguage.getName(),
                        currentLanguage.getIsoCode6391(),
                        currentLanguage.getIsoCode6392(),
                        FILE_WRONG_FORMAT));
        }
        
        HashMap<String,String> string = null;
        string = parseResourcesDocument(doc);
        if (string != null) {
            R.string = string;
        }
        else throw new ResourceReadException(String.format(LOAD_STRINGS_MESSAGE,
                        FAILED_PROC,
                        currentLanguage.getName(),
                        currentLanguage.getIsoCode6391(),
                        currentLanguage.getIsoCode6392(),
                        FILE_WRONG_STRUCTURE));
        
    }
    private static Document readXmlDocument(File xml) throws SAXException, IOException, ParserConfigurationException{
        DocumentBuilderFactory dbfac = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = dbfac.newDocumentBuilder();
        return docBuilder.parse(xml);
    }
    private static HashMap<String,String> parseResourcesDocument(Document strings){
        NodeList rootMatches = strings.getElementsByTagName(RESOURCES_ROOT);
        HashMap<String,String> coincidences = null;
        if (rootMatches.getLength() == 1) {
            Element rootElement = (Element)rootMatches.item(0);
            NodeList childs = rootElement.getChildNodes();
            for (int counter = 0; counter < childs.getLength(); counter++) {
                Node currentNode = childs.item(counter);
                if (currentNode instanceof Element){
                    Element currentElement = (Element)currentNode;
                    String elementType = currentElement.getTagName();
                    if (elementType.equals(STRINGS_TAG)
                            ||
                        elementType.equals(IMAGES_TAG)) {
                        String elementKey = currentElement.getAttribute(KEY_ATTR);
                        String elementText = currentElement.getTextContent();
                        if (coincidences == null) coincidences = new HashMap<>();
                        coincidences.put(elementKey, elementText);
                        System.out.printf(RESOURCE_READ,elementType,elementKey,elementText);
                    }
                    else{
                        System.err.printf(WARNING_BAD_TAG,elementType);
                    }
                }
            }
        }
        return coincidences;
    }

    public static Language getCurrentLanguage() {
        return currentLanguage;
    }
    public static void setLanguage(Language language) throws ResourceReadException{
        if (language != null) {
            cleanStrings();
            loadStrings(language);
        }
    }
    public static String getString(String key){
        return string.getOrDefault(key,DEFAULT_STRING);
    }
    public static String getDataString(String key){
        return R.data.getOrDefault(key,DEFAULT_STRING);
    }
    public static ImageResource getImage(String key){
        return images.getOrDefault(key,DEFAULT_IMAGE);
    }
    public static void setString(Labeled form, String key){
        form.setText(getString(key));
    }
    public static void setData(Labeled form, String key){
        form.setText(getString(key));
    }
}
