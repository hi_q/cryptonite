/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cryptonite.res;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Luciano
 */
public abstract class Resource {
    
    //base archivos de recursos
    // usada anteriormente con variaciones en Street Fighter Hangman
    
    private String location;
    private String key = "";
    

    public Resource(String key, String path){
        this.key = key;
        File file = new File(path);
        if (file.exists()) {
            location = path;
        }
        else{
            System.err.println("ERROR. Resource "+path+" not found.");
        }
        
    }
    
    public String getKey() {
        return key;
    }
    public String getLocation() {
        return location;
    }
}
