/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cryptonite.res;

/**
 *
 * @author Luciano
 */
public class ResourceReadException extends Exception{
    // lanzado tras un error de lectura de resources
    public ResourceReadException() {
    }

    public ResourceReadException(String message) {
        super(message);
    }
    
}
