/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cryptonite.res;

/**
 *
 * @author Luciano
 */
public enum Language {
    
    // enum posibles idiomas
    
    English("English","en","eng"),
    Spanish("Español","es","spa"),
    Catalan("Català","ca","cat");
    
    private final String name;
    private final String isoCode6391;
    private final String isoCode6392;

    private Language(String language, String iso6391, String iso6392) {
        this.name = language;
        this.isoCode6391 = iso6391;
        this.isoCode6392 = iso6392;
    }
    public String getName() {
        return name;
    }
    public String getIsoCode6391() {
        return isoCode6391;
    }
    public static Language findByIsoCode6391(String key){
        for (Language value : Language.values()) {
            if (key.equals(value.getIsoCode6391()))
                return value;
        }
        return null;
    }
    public static Language findByLanguageName(String key){
        for (Language value : Language.values()) {
            if (key.equals(value.getName()))
                return value;
        }
        return null;
    }
    public String getIsoCode6392() {
        return isoCode6392;
    }
    
    
}
