/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cryptonite.utils;

import java.security.NoSuchAlgorithmException;
import javax.crypto.NoSuchPaddingException;

/**
 *
 * @author Luciano
 */
public interface Generation {
    // el proceso de codificacion saldrá en el process, por eso es abstracto
    public abstract void process() throws NoSuchAlgorithmException, NoSuchPaddingException;
}
