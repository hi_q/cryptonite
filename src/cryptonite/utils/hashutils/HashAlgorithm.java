/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cryptonite.utils.hashutils;

/**
 *
 * @author Luciano
 */
public enum HashAlgorithm {
    //declaracion de posibles algoritmos
    MD5("MD5","Message Digest Algorithm 5"),
    SHA1("SHA-1","Secure Hash Algorithm 1"),
    SHA256("SHA-256","Secure Hash Algorithm (256 bits)"),
    SHA512("SHA-512","Secure Hash Algorithm (512 bits)");
    
    private final String name;
    private final String longName;

    private HashAlgorithm(String name, String longName) {
        this.name = name;
        this.longName = longName;
    }

    public String getName() {
        return name;
    }
    public String getLongName() {
        return longName;
    }
    
    public static HashAlgorithm findByName(String name){
        for (HashAlgorithm value : HashAlgorithm.values()) {
            if (value.getName().equals(name)) 
                return value;
        }
        return null;
    }
}