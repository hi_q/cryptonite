/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cryptonite.utils.cryptoutils;

import cryptonite.utils.Encryptable;
import cryptonite.utils.Process;
import cryptonite.utils.hashutils.HashAlgorithm;
import cryptonite.utils.hashutils.HashProcess;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.CipherOutputStream;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
/**
 *
 * @author Luciano
 */
public class CryptoAESProcess extends Process<CryptoSymmetricAlgorithm> implements Encryptable{
    
    public CryptoAESProcess(byte[] message) {
        super(message, CryptoSymmetricAlgorithm.AES);
    }

    @Override
    public void encrypt(String password) {
        try {
            byte[] encrypted = encryptProc(roundUpToValid(message),password);
            messageProcessed = encrypted;
            
        } catch (Exception ex) {
            Logger.getLogger(CryptoAESProcess.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    @Override
    public void decrypt(String password) {
        try {
            byte[] decrypted = decryptProc(message,password);
            messageProcessed = decrypted;
        } catch (Exception ex) {
            Logger.getLogger(CryptoAESProcess.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private byte[] encryptProc(byte[] text, String password) throws Exception {

        // Em preparo un objecte sense instanciar de la clau secreta 
        SecretKey key;
        
        // Obtenir un objecte tipus clau secreta a partir d'un password
        key = passwordKeyGeneration(password, 128);
        // Obtinc l'objecte que encripta amb AES
        Cipher aesCipher = Cipher.getInstance("AES");
        // Inicialitzo l'objecte d'encriptació AES amb la clau
        aesCipher.init(Cipher.ENCRYPT_MODE, key);

        // Fitxer d'entrada que volem encriptar
        ByteArrayInputStream fis = new ByteArrayInputStream(text);
        // fitxer de sortida xifrat. Tindrà extensió .aes
        ByteArrayOutputStream fos = new ByteArrayOutputStream();
        // Associació del canal de sortida per a xifrar (establim la sortida i el mètode xifrador emprat)
        CipherOutputStream cos = new CipherOutputStream(fos, aesCipher);

        // Llegim el fitxer binari i anem xifrant amb blocs utilitzant l'instància de CipherOutputStream
        int read;
        byte buf[] = new byte[16];
        while ((read = fis.read(buf)) != -1) //Llegim les dades en blocs
        {
            cos.write(buf, 0, read); //i les xifrem i escrivim al fitxer
        }
        
        byte[] toRet =  (fos.toByteArray());
        
        fis.close();
        fos.close();
        cos.close();
        return toRet;
    }
    private byte[] decryptProc(byte[] encodedText, String password) throws Exception {

        // Em preparo un objecte sense instanciar de la clau secreta 
        SecretKey key;
        
        // Obtenir un objecte tipus clau secreta a partir d'un password
        key = passwordKeyGeneration(password, 128);
        // Obtinc l'objecte que encripta amb AES
        Cipher aesCipher = Cipher.getInstance("AES");
        // Inicialitzo l'objecte d'encriptació AES amb la clau
        aesCipher.init(Cipher.DECRYPT_MODE, key);

        // Fitxer d'entrada que volem encriptar
        ByteArrayInputStream fis = new ByteArrayInputStream(encodedText);
        // fitxer de sortida xifrat. Tindrà extensió .aes
        ByteArrayOutputStream fos = new ByteArrayOutputStream();
        // Associació del canal de sortida per a xifrar (establim la sortida i el mètode xifrador emprat)
        CipherOutputStream cos = new CipherOutputStream(fos, aesCipher);

        // Llegim el fitxer binari i anem xifrant amb blocs utilitzant l'instància de CipherOutputStream
        int read;
        byte buf[] = new byte[16];
        while ((read = fis.read(buf)) != -1) //Llegim les dades en blocs
        {
            cos.write(buf, 0, read); //i les xifrem i escrivim al fitxer
        }
        
        byte[] res = fos.toByteArray();
        
        fis.close();
        fos.close();
        cos.close();
        return res;
    }
    
    public static SecretKey passwordKeyGeneration(String text, int keySize) {
        SecretKey sKey = null;
        // Si no es del tamany existent classe KeyGenerator  https://docs.oracle.com/javase/7/docs/api/javax/crypto/KeyGenerator.html 
        if ((keySize == 128) || (keySize == 192) || (keySize == 256)) {
            try {
                // Conversió password string a binàri
                byte[] data = text.getBytes("UTF-8");
                // Faig el procés de hash establint l'algorisme
                MessageDigest md = MessageDigest.getInstance("MD5");
                // Es fa un Hash del password
                byte[] hash = md.digest(data);
                // Ara, per a generar la SecretKey, agafo els 32 (256/8) primers bytes del hash del password
                byte[] key = Arrays.copyOf(hash, keySize / 8);
                // finalment l'estableixo
                sKey = new SecretKeySpec(key, "AES");
            } catch (UnsupportedEncodingException | NoSuchAlgorithmException ex) {
                System.err.println("Error de generació de clau:" + ex);
            }
        }
        return sKey;
    }
    
    
    private static byte[] roundUpToValid(byte[] bytes){
        return Arrays.copyOf(bytes, roundNumberTo(bytes.length,16)+16);
    }
    private static int roundNumberTo(int n, int aim) {
        int constant = aim;
        while((aim - n) < 0){
            aim+=constant;
        }
        int rest = aim-n;
        return n+rest;
    }

    @Override
    public String getMessageProcessedAsString() {
        return new String(messageProcessed);
    }
}
