/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cryptonite.utils.cryptoutils;

/**
 *
 * @author Luciano
 */
public enum CryptoSymmetricAlgorithm {
    AES("AES","Advanced Encryption Standard");
    
    private String name;
    private String longName;

    private CryptoSymmetricAlgorithm(String name, String longName) {
        this.name = name;
        this.longName = longName;
    }

    public String getName() {
        return name;
    }

    public String getLongName() {
        return longName;
    }
    
    
}
