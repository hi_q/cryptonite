/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cryptonite.utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.NoSuchPaddingException;

/**
 *
 * @author Luciano
 */
public abstract class Process<A> {
    // PROCESO - clase abstracta - independiente del algoritmo
    protected A algorithm = null;
    protected byte[] message = null;
    protected byte[] messageProcessed = null;
    
    public Process(String message, A algorithm) {
        this(message.getBytes(),algorithm);
    }
    public Process(byte[] message, A algorithm) {
        this.message = message;
        this.algorithm = algorithm;
    }

    public byte[] getMessage() {
        return message;
    }
    public String getMessageAsString(){
        return new String(getMessage());
    }
    public byte[] getMessageProcessed() {
        return messageProcessed;
    }
    public abstract String getMessageProcessedAsString();
    
    protected String byteArrayToHexadecimalString(byte[] bytes){
        StringBuilder builder = new StringBuilder();
            for (byte thisByte : bytes)
                builder.append(String.format("%02x", thisByte & 0xff));
        return builder.toString();
    }
    
    public A getAlgorithm() {
        return algorithm;
    }    
    public void setAlgorithm(A algorithm){
        if (algorithm != null) {
            this.algorithm = algorithm;
            messageProcessed = null;
        }
    }
}
