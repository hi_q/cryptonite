/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cryptonite.utils;

import java.security.NoSuchAlgorithmException;
import javax.crypto.NoSuchPaddingException;

/**
 *
 * @author Luciano
 */
public interface Encryptable {
    // encriptar y desencriptar, obligatorio para cualquier encriptable (CryptoAESProcess)
    public abstract void encrypt(String password);
    public abstract void decrypt(String password);
}
