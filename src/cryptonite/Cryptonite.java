/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cryptonite;

import com.sun.javafx.css.Size;
import cryptonite.controller.CryptoViewController;
import cryptonite.controller.HashViewController;
import cryptonite.controller.MainViewController;
import cryptonite.controller.MenuViewController;
import cryptonite.utils.hashutils.HashAlgorithm;
import cryptonite.utils.hashutils.HashProcess;
import cryptonite.res.Language;
import cryptonite.res.ResourceReadException;
import cryptonite.res.R;
import cryptonite.utils.cryptoutils.CryptoSymmetricAlgorithm;
import cryptonite.utils.cryptoutils.CryptoAESProcess;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 *
 * @author Luciano
 */
public class Cryptonite extends Application {
    private static Stage stage;
    private static File defaultLanguageFile = new File("config.ini");
    private static Size size;
    private static final double[] MAIN_DIMENSIONS = {500,300};
    private static final double[] MENU_DIMENSIONS = {720,540};
    private static final double[] OTHER_DIMENSIONS = {900,675};
    private static boolean ignoreMainStage = false;
    
    // MAIN (DETECTAR IDIOMAS Y ARRANCAR PRIMER STAGE EN FUNCION DEL DETECTADO)
    public static void main(String[] args) throws ResourceReadException {
        byte[] in = "ENCRYPTION PROCESS FIRST TRY. You should be able to read this message if all ok.".getBytes();
        CryptoAESProcess process = new CryptoAESProcess(in);
        process.encrypt("password");
        byte[] code = process.getMessageProcessed();
        CryptoAESProcess decoder = new CryptoAESProcess(code);
        decoder.decrypt("password");
        System.out.println(decoder.getMessageProcessedAsString());
        
        
        Language defLang = readDefaultLanguage();
        if (defLang != null){
            System.out.printf("DEFAULT LANGUAGE DETECTED (%s). Loading resources above:\n",defLang.getIsoCode6391());
            R.setLanguage(defLang);
            ignoreMainStage = true;
        }
        launch(args);
    }
    
    // PREPARACION DE STAGE
    @Override
    public void start(Stage stage) throws Exception {
        Cryptonite.stage = stage;
        Cryptonite.stage.setTitle(R.getDataString("app_name"));
        Cryptonite.stage.setResizable(false);
        Cryptonite.stage.getIcons().add(R.getImage("logo").getAsImage());
        Cryptonite.stage.initStyle(StageStyle.UNDECORATED);
        if (!ignoreMainStage) {
            startMainStage();
        }
        else{
            startMenuStage();
        }
    }
    
    //OPERACION CERRAR STAGE (OBLIGATORIO ANTES DE ABRIR OTRO)
    public static void closeStage(){
        Cryptonite.stage.close();
    }
    
    //OPERACIONES ABRIR STAGE
    public static void startMainStage() throws IOException{
        FXMLLoader view = new FXMLLoader(Cryptonite.class.getResource("view/MainView.fxml"));
        
        MainViewController controller = new MainViewController();
        
        view.setController(controller);
        Cryptonite.stage.setWidth(MAIN_DIMENSIONS[0]);
        Cryptonite.stage.setHeight(MAIN_DIMENSIONS[1]);
        Cryptonite.stage.setResizable(false);
        
        Scene scene = new Scene(view.load());
        stage.setScene(scene);
        stage.show();
    }
    public static void startMenuStage() throws Exception{
        FXMLLoader view = new FXMLLoader(Cryptonite.class.getResource("view/MenuView.fxml"));
        
        MenuViewController controller = new MenuViewController();
        
        view.setController(controller);
        Cryptonite.stage.setResizable(false);
        Cryptonite.stage.setWidth(MENU_DIMENSIONS[0]);
        Cryptonite.stage.setHeight(MENU_DIMENSIONS[1]);
        
        Scene scene = new Scene(view.load());
        stage.setScene(scene);
        stage.show();
    }
    public static void startHashStage() throws Exception{
        FXMLLoader view = new FXMLLoader(Cryptonite.class.getResource("view/HashView.fxml"));
        
        HashViewController controller = new HashViewController();
        
        view.setController(controller);
        Cryptonite.stage.setWidth(OTHER_DIMENSIONS[0]);
        Cryptonite.stage.setHeight(OTHER_DIMENSIONS[1]);
        
        Scene scene = new Scene(view.load());
        stage.setScene(scene);
        stage.show();
    }
    public static void startCryptoStage() throws Exception{
        FXMLLoader view = new FXMLLoader(Cryptonite.class.getResource("view/CryptoView.fxml"));
        
        CryptoViewController controller = new CryptoViewController();
        
        view.setController(controller);
        Cryptonite.stage.setWidth(OTHER_DIMENSIONS[0]);
        Cryptonite.stage.setHeight(OTHER_DIMENSIONS[1]);
        
        Scene scene = new Scene(view.load());
        stage.setScene(scene);
        stage.show();
    }
    
    // OBTENER EL STAGE DESDE OTROS LUGARES
    public static Stage getStage() {
        return stage;
    }

    // OBTENER VARIABLE QUE IGNORA EL STAGE DE SELECCION DE IDIOMA
    public static boolean isIgnoreMainStage() {
        return ignoreMainStage;
    }
    
    // LECTURA/ESCRITURA DE LENGUAJE POR DEFECTO
    public static Language readDefaultLanguage(){
        if (!defaultLanguageFile.exists()) return null;
        else{
            try {
                BufferedReader reader = new BufferedReader(new FileReader(defaultLanguageFile));
                String iso = reader.readLine();
                for (Language value : Language.values()) {
                    if (iso.equals(value.getIsoCode6391())) return value;
                }
                return null;
            } catch (IOException ex) {
                Logger.getLogger(Cryptonite.class.getName()).log(Level.SEVERE, null, ex);
                return null;
            }
        }
    }
    public static void writeDefaultLanguage(Language language){
        try {
            if (!defaultLanguageFile.exists()) 
                defaultLanguageFile.createNewFile();
            else{
                PrintWriter writer = new PrintWriter(defaultLanguageFile); // eliminar contenido
                writer.print("");
                writer.close();
            }
            BufferedWriter writer = new BufferedWriter(new FileWriter(defaultLanguageFile));
            writer.write(language.getIsoCode6391());
            System.out.println("CRYPTONITE - DEFAULT LANGUAGE WRITTEN ("+language.getIsoCode6391()+")");
            writer.newLine();
            writer.close();
        } catch (IOException ex) {
            Logger.getLogger(Cryptonite.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
